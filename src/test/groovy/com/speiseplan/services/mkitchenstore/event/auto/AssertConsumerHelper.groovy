package com.speiseplan.services.mkitchenstore.event.auto

import org.springframework.amqp.rabbit.core.RabbitTemplate

/**
 * Created by pakito on 9/06/17.
 */
class AssertConsumerHelper {

    public static void what(RabbitTemplate rabbitJsonTemplate, Integer totalMessagesConsumed, Boolean consume){

        if (!consume){
            return
        }

        def eventOperation = true
        List<Map> listened = []
        while (eventOperation) {
            eventOperation = (rabbitJsonTemplate.receiveAndConvert("mkitchenstore.rdbms") as Map)
            if ( eventOperation){
                println eventOperation
                listened << eventOperation
            }

        }

        assert listened
        assert listened.size() == totalMessagesConsumed
    }
}
