package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Location
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["rabbitmq-local", "test", "hsql", "events", "audit","populate-owners", "no-security"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
//@Ignore("needs a running rabbitmq to be tested")
@Category(RabbitMQIntegrationTests.class)
class DuplicatedOriginEventRabbitTest {

    @Autowired
    private EmbeddedWebApplicationContext server

    @Autowired
    private EmbeddedServletContainerFactory bean

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private FoodPointRepository foodPointRepository


    @Autowired
    private OwnerRepository ownerRepository

    @Autowired
    RabbitTemplate rabbitJsonTemplate

    @Autowired
    private RabbitAdmin rabbitAdmin

    @Autowired
    private RestTemplate httpTemplate // by name

    @Value('${local.server.port}')
    int port

    @Value('${spring.application.name}')
    String applicationName

    @Value('${mkitchenstore.test.consumeMessages}')
    private Boolean consumeMessages

    Traverson traverson

    @Before
    public void setUp(){
        (applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
            rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
        }
    }

    @Test
    void server_should_be_running() {
        assert server
        assert applicationContext
        assert port
    }

    @Test
    void create_and_update_food_point_should_create_mensajes() {
        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)


        def headers = new HttpHeaders()

        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

        int decorator = 1
        int numberEvents = 6 // pre/post save pre/post update pre/post delete

        String ownerLink = traverson.
                follow("owners","search","byId").
                withTemplateParameters([id : ownerRepository.findAll().first().pk]).
                toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
                getLink("self").
                getHref();

        assert ownerLink

        (1..decorator).each {

            // Create food point
            Map<String,Object> foodPoint = [
                    name       : "notNullName${new Random().nextInt()}" as String,
                    displayName: "notNullDisplayName",
                    owner      : ownerLink,
                    location   : new Location(latitude:19.432608f , longitude:-99.133208f )
            ]
            def created = httpTemplate.exchange(
                    traverson.follow("foodpoints").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(foodPoint, headers),
                    new ParameterizedTypeReference<Resource<FoodPoint>>() {}
            )
            assert created
            assert created.statusCode == HttpStatus.CREATED

            // update food point
            FoodPoint toUpdate = created.getBody().getContent()
            toUpdate.displayName += "_UPDATED"

            def updated = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.PUT,
                    new HttpEntity<FoodPoint>(toUpdate, headers),
                    new ParameterizedTypeReference<Resource<FoodPoint>>() {}
            )

            assert updated

            // delete food point
            def deleted = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.DELETE,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<Resource<?>>() {}
            )

            assert deleted
        }

        // assert the messages were sent and we can receive them
        AssertConsumerHelper.what(rabbitJsonTemplate, decorator * numberEvents, consumeMessages)
    }

}
