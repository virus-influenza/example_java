package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.config.Application

//import com.speiseplan.domain.mkitchencontext.resource.types.Categories
import com.speiseplan.services.mkitchenstore.domain.*

//import com.speiseplan.services.mkitchenstore.domain.Contact
//import com.speiseplan.services.mkitchenstore.domain.Info
import com.speiseplan.services.mkitchenstore.repository.FoodCategoryRepository
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.repository.PlateRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "rabbitmq-local","hsql-local", "no-security", "populate-prod", "events", "audit",])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
// @Ignore("needs a running rabbitmq to be tested")
@org.junit.experimental.categories.Category(RabbitMQIntegrationTests.class)
class OfferCollectionEventHandlerTest {
    
    private static final int DECORATOR = 1

    @Autowired
    private EmbeddedWebApplicationContext server
    @Autowired
    private EmbeddedServletContainerFactory bean
    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private FoodPointRepository foodPointRepository    
    @Autowired
    private MenuRepository menuRepository
    @Autowired
    private PlateRepository plateRepository
    @Autowired
    private FoodCategoryRepository foodCategoryRepository
    @Autowired
    OwnerRepository ownerRepository

    @Autowired
    private RabbitAdmin rabbitAdmin

    @Autowired
    private RabbitTemplate rabbitJsonTemplate
    @Autowired
    private RestTemplate httpTemplate // by name

    @Value('${local.server.port}')
    private int port
    @Value('${spring.application.name}')
    private String applicationName
    @Value('${mkitchenstore.test.consumeMessages}')
    private Boolean consumeMessages


    Traverson traverson

    @Before
    public void setUp(){
        (applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
            rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
        }
    }

    @Test
    void create_and_update_menu_should_create_messages() {

        int NUMBER_OF_EVENTS = 6 // pre post save, pre post update, pre post delete

        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

        def headers = new HttpHeaders()
        headers.setContentType(MediaTypes.HAL_JSON)
        headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

        Map<String,Object> toPost
        (1..DECORATOR).each {
            toPost = [
                    name       : "notNullName${new Random().nextInt()}" as String,
                    displayName: "notNullDisplayName",
                    owner      : traverson.
                            follow("owners","search","byId").
                            withTemplateParameters([id : ownerRepository.findAll().first().pk]).
                            toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
                            getLink("self").
                            getHref()
            ]


            def created = httpTemplate.exchange(
                    traverson.follow("menus").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(toPost, headers),
                    new ParameterizedTypeReference<Resource<Menu>>() {}
            )


            assert created
            assert created.getStatusCode() == HttpStatus.CREATED

            // simple update
            Menu toUpdate = created.getBody().getContent()
            toUpdate.displayName += "_UPDATED"

            def updated = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.PUT,
                    new HttpEntity<Menu>(toUpdate, headers),
                    new ParameterizedTypeReference<Resource<Menu>>() {}
            )

            assert updated

            def deleted = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.DELETE,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<Resource<?>>() {}
            )

            assert deleted
        }

        AssertConsumerHelper.what(rabbitJsonTemplate, DECORATOR * NUMBER_OF_EVENTS, consumeMessages)

    }

}
