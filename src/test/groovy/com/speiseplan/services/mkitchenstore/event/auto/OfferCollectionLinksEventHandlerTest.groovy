package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.config.Application

//import com.speiseplan.domain.mkitchencontext.resource.types.Categories
import com.speiseplan.services.mkitchenstore.domain.*
import com.speiseplan.services.mkitchenstore.domain.types.Categories

//import com.speiseplan.services.mkitchenstore.domain.Contact
//import com.speiseplan.services.mkitchenstore.domain.Info
import com.speiseplan.services.mkitchenstore.repository.FoodCategoryRepository
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.repository.PlateRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "rabbitmq-local", "hsql-local", "no-security", "populate-owners", "events", "audit",])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@Ignore("needs a running rabbitmq to be tested")
@org.junit.experimental.categories.Category(RabbitMQIntegrationTests.class)

class OfferCollectionLinksEventHandlerTest {

    @Autowired
    private EmbeddedWebApplicationContext server
    @Autowired
    private EmbeddedServletContainerFactory bean
    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private FoodPointRepository foodPointRepository
    @Autowired
    private MenuRepository menuRepository
    @Autowired
    private PlateRepository plateRepository
    @Autowired
    private FoodCategoryRepository foodCategoryRepository
    @Autowired
    OwnerRepository ownerRepository


    @Autowired
    private RabbitTemplate rabbitJsonTemplate
    @Autowired
    private RestTemplate httpTemplate // by name

    @Value('${local.server.port}')
    private int port
    @Value('${spring.application.name}')
    private String applicationName
    @Value('${mkitchenstore.test.consumeMessages}')
    private Boolean consumeMessages

    @Autowired
    private RabbitAdmin rabbitAdmin

    Traverson traverson


    @Before
    public void setUp(){
        (applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
            rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
        }
    }
    @Test
    void create_and_update_menu_should_create_messages() {

        int NUMBER_OF_EVENTS = 6 // pre post save, pre post update, pre post delete

        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

        def headers = new HttpHeaders()
        headers.setContentType(MediaTypes.HAL_JSON)
        headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

        Map<String, Object> toPost

            toPost = [
                    name       : "notNullName${new Random().nextInt()}" as String,
                    displayName: "notNullDisplayName",
                    owner      : traverson.
                            follow("owners", "search", "byId").
                            withTemplateParameters([id: ownerRepository.findAll().first().pk]).
                            toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
                            getLink("self").
                            getHref()
            ]


            def created = httpTemplate.exchange(
                    traverson.follow("menus").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(toPost, headers),
                    new ParameterizedTypeReference<Resource<Menu>>() {}
            )

            assert created
            assert created.getStatusCode() == HttpStatus.CREATED

            // simple update
            Menu toUpdate = created.getBody().getContent()
            toUpdate.displayName += "_UPDATED"

            def updated = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.PUT,
                    new HttpEntity<Menu>(toUpdate, headers),
                    new ParameterizedTypeReference<Resource<Menu>>() {}
            )

            assert updated

            def deleted = httpTemplate.exchange(
                    new URI(created.getBody().getLink("self").getHref()),
                    HttpMethod.DELETE,
                    new HttpEntity(headers),
                    new ParameterizedTypeReference<Resource<?>>() {}
            )

            assert deleted

        AssertConsumerHelper.what(rabbitJsonTemplate, NUMBER_OF_EVENTS, consumeMessages)

    }


    @Test
    void create_and_update_menu_with_categories_should_create_messages() {
        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

        def halJson = new HttpHeaders()

        halJson.setContentType(MediaTypes.HAL_JSON)
        halJson.setAccept([MediaTypes.HAL_JSON].asImmutable())

        def uriList = new HttpHeaders()

        uriList.setContentType(RestMediaTypes.TEXT_URI_LIST)
        uriList.setAccept([MediaTypes.HAL_JSON].asImmutable())

        // create menu
        Map<String, Object> toPost = [
                name       : "Mrs_Merkel_Menu_Number-${new Random().nextInt()}" as String,
                displayName: "Mrs Merkel Menu Number-${new Random().nextInt()}" as String,
                owner      : traverson.
                        follow("owners", "search", "byId").
                        withTemplateParameters([id: ownerRepository.findAll().first().pk]).
                        toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
                        getLink("self").
                        getHref()

        ]
        def menuCreated = httpTemplate.exchange(
                new URI("http://localhost:${port}/menus/"),
                HttpMethod.POST,
                new HttpEntity(toPost, halJson),
                new ParameterizedTypeReference<Resource<Menu>>() {}
        )
        assert menuCreated
        assert menuCreated.getStatusCode() == HttpStatus.CREATED

        // create multiple associations
        // 2) Category
        ["VEGAN", "ITALIAN", "GREEK", "NORDIC", "CARIBBEAN", "SPANISH", "MEXICAN", "CHEAP", "BURRITO", "YOQUESE"].each {

            // create sub resource
            Category toPostSubResource = new Category([
                    name         : it as String,
                    displayName  : it as String,
                    offerCategory: Categories.values()[new Random().nextInt(Categories.values().size())]
            ])

            def categoryCreated = httpTemplate.exchange(
                    new URI("http://localhost:${port}/categories/"),
                    HttpMethod.POST,
                    new HttpEntity<Category>(toPostSubResource, halJson),
                    new ParameterizedTypeReference<Resource<Category>>() {}
            )
            assert categoryCreated
            assert categoryCreated.getStatusCode() == HttpStatus.CREATED

            // associate
            def associated = httpTemplate.exchange(
                    new URI(menuCreated.getBody().getLink("categories").getHref()),
                    HttpMethod.PATCH,
                    new HttpEntity<String>(categoryCreated.getBody().getLink("self").getHref(), uriList),
                    new ParameterizedTypeReference<Resource<?>>() {}
            )

            assert associated


        }

        // delete
        def menuCategories = httpTemplate.exchange(
                new URI(menuCreated.getBody().getLink("categories").getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(halJson),
                new ParameterizedTypeReference<Resources<Resource<Category>>>() {}
        )

        menuCategories.getBody().each { Resource<Category> categoryResource ->

            def deleted = httpTemplate.exchange(
                    new URI(menuCreated.getBody().getLink("categories").getHref() + "/${categoryResource.getContent().getPk()}"),
                    HttpMethod.DELETE,
                    new HttpEntity<Object>(halJson),
                    new ParameterizedTypeReference<Resource<?>>() {}
            )

            assert deleted
            assert deleted.getStatusCode() == HttpStatus.NO_CONTENT

        }

        // 2 pre post create menu
        // 10 Categories, pre post create category, post link create menu category, post link delete menu category
        AssertConsumerHelper.what(rabbitJsonTemplate, 2 + ( 10 * 4) , consumeMessages)
    }

}
