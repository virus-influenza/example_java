package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.*
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import org.springframework.test.annotation.DirtiesContext

import org.junit.experimental.categories.Category

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 *
 * @author nak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "rabbitmq-local","hsql-local", "no-security", "populate-prod", "events", "audit"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
//@Ignore("needs a running rabbitmq to be tested")
// https://www.safaribooksonline.com/blog/2013/08/22/gradle-test-organization/
@Category(RabbitMQIntegrationTests.class)
class ActivationEventHandlerTest {

	private static final int DECORATOR = 1

	@Autowired
	private EmbeddedWebApplicationContext server
	@Autowired
	private EmbeddedServletContainerFactory bean
	@Autowired
	private ApplicationContext applicationContext
	@Autowired
	private RabbitAdmin rabbitAdmin

	@Autowired
	private RabbitTemplate rabbitJsonTemplate
	@Autowired
	private RestTemplate httpTemplate // by name
	@Autowired
	OwnerRepository ownerRepository

	@Value('${local.server.port}')
	private int port
	@Value('${spring.application.name}')
	private String applicationName
	@Value('${mkitchenstore.test.consumeMessages}')
	private Boolean consumeMessages

	Traverson traverson

	@Before
	public void setUp(){
		(applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
			rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
		}
	}

	@Test
	void create_and_update_and_delete_activation_should_create_messages() {

		int NUMBER_OF_EVENTS = 8 // pre post save (menu), pre post save (activation), pre post update (activation), pre post delete (activation)


		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

		def headers = new HttpHeaders()
		headers.setContentType(MediaTypes.HAL_JSON)
		headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

		Map<String,Object> toPost
		(1..DECORATOR).each {

			String ownerLink = traverson.
					follow("owners","search","byId").
					withTemplateParameters([id : ownerRepository.findAll().first().pk]).
					toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
					getLink("self").
					getHref();

			assert ownerLink

			def menuPost = [
				name       : "notNullName${new Random().nextInt()}" as String,
				displayName: "notNullDisplayName",
				owner      : ownerLink
			]

			def menuCreated = httpTemplate.exchange(
					traverson.follow("menus").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(menuPost, headers),
					new ParameterizedTypeReference<Resource<Menu>>() {}
					)
			assert menuCreated

			toPost = [
				start    	    : LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				end             : LocalDate.now().plusDays(20).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				location        : [latitude   : new Random().nextDouble(),
								   longitude  : new Random().nextDouble()],
				owner           : ownerLink,
				offerCollection : menuCreated.getBody().getLink("self").getHref()
			]


			def created = httpTemplate.exchange(
					traverson.follow("activations").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(toPost, headers),
					new ParameterizedTypeReference<Resource<Activation>>() {}
					)


			assert created
			assert created.getStatusCode() == HttpStatus.CREATED

			// simple update
			Activation responseEntity = created.getBody().getContent()
			responseEntity.end = LocalDate.now().plusDays(25)

			def updated = httpTemplate.exchange(
					new URI("http://localhost:${port}/activations/${responseEntity.pk}"),
					HttpMethod.PUT,
					new HttpEntity<Activation>(responseEntity, headers),
					new ParameterizedTypeReference<Resource<Activation>>() {}
					)

			assert updated
			assert updated.getStatusCode() == HttpStatus.OK

			// delete resource
			def delete = httpTemplate.exchange(
					new URI("http://localhost:${port}/activations/${responseEntity.pk}"),
					HttpMethod.DELETE,
					new HttpEntity<Activation>(responseEntity, headers),
					new ParameterizedTypeReference<Resource<Activation>>() {}
					)
			assert delete
			assert delete.getStatusCode() == HttpStatus.NO_CONTENT


		}

		// assert the messages were sent and we can receive them
		AssertConsumerHelper.what(rabbitJsonTemplate, DECORATOR * NUMBER_OF_EVENTS, consumeMessages)
	}

	@Test
	void create_and_associate_origin_should_create_messages() {
		int NUMBER_OF_EVENTS = 8 // pre post save (menu), pre post save (activation), pre post save ( food point) pre post delete ( activation )

		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

		def headers = new HttpHeaders()
		headers.setContentType(MediaTypes.HAL_JSON)
		headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

		def uriList = new HttpHeaders()
		uriList.setContentType(RestMediaTypes.TEXT_URI_LIST)
		uriList.setAccept([MediaTypes.HAL_JSON].asImmutable())

		Map<String,Object> toPost
		(1..DECORATOR).each {

			// get owner from database
			String ownerLink = traverson.
					follow("owners","search","byId").
					withTemplateParameters([id : ownerRepository.findAll().first().pk]).
					toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
					getLink("self").
					getHref();

			assert ownerLink

			// create menu
			def menuPost = [
				name       : "notNullName${new Random().nextInt()}" as String,
				displayName: "notNullDisplayName",
				owner      : ownerLink
			]

			def menuCreated = httpTemplate.exchange(
					traverson.follow("menus").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(menuPost, headers),
					new ParameterizedTypeReference<Resource<Menu>>() {}
					)
			assert menuCreated

			// 2) An Origin
			Map<String,Object> foodPoint = [
				name       : "notNullName${new Random().nextInt()}" as String,
				displayName: "notNullDisplayName",
				owner      : ownerLink,
				location   : new Location(latitude:19.432608f , longitude:-99.133208f )
			]
			def foodPointPostResponse = httpTemplate.exchange(
					traverson.follow("foodpoints").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(foodPoint, headers),
					new ParameterizedTypeReference<Resource<FoodPoint>>() {}
					)
			assert foodPointPostResponse
			assert foodPointPostResponse.statusCode == HttpStatus.CREATED

			// create activation without origins
			toPost = [
				start    	    : LocalDate.now().format(DateTimeFormatter.ofPattern('dd/MM/yyyy')),
				end             : LocalDate.now().plusDays(20).format(DateTimeFormatter.ofPattern('dd/MM/yyyy')),
				location        : [latitude   : new Random().nextDouble(),
					longitude  : new Random().nextDouble()],
				origin			: foodPointPostResponse.getBody().getLink("self").getHref(),
				owner           : ownerLink,
				offerCollection : menuCreated.getBody().getLink("self").getHref()
			]
			def created = httpTemplate.exchange(
					traverson.follow("activations").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(toPost, headers),
					new ParameterizedTypeReference<Resource<Activation>>() {}
					)
			assert created
			assert created.getStatusCode() == HttpStatus.CREATED

			def delete = httpTemplate.exchange(
					new URI(created.getBody().getLink("self").getHref()),
					HttpMethod.DELETE,
					new HttpEntity(headers),
					new ParameterizedTypeReference<Resource<?>>() {}
					)
			assert delete
			assert delete.getStatusCode() == HttpStatus.NO_CONTENT

		}

		AssertConsumerHelper.what(rabbitJsonTemplate, DECORATOR * NUMBER_OF_EVENTS, consumeMessages)
	}

}
