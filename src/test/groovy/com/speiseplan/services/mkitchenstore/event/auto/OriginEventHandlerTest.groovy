package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.*
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import org.springframework.test.annotation.DirtiesContext

/**
 *
 * @author nak
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "rabbitmq-local","hsql-local", "no-security", "populate-prod", "events", "audit"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
//@Ignore("needs a running rabbitmq to be tested")
@org.junit.experimental.categories.Category(RabbitMQIntegrationTests.class)

class OriginEventHandlerTest {

	private static final int DECORATOR = 1
	private static final int NUMBER_OF_EVENTS = 6 // pre post save, pre post update, pre post delete

	@Autowired
	private EmbeddedWebApplicationContext server
	@Autowired
	private EmbeddedServletContainerFactory bean
	@Autowired
	private ApplicationContext applicationContext
	@Autowired
	private RabbitAdmin rabbitAdmin

	@Autowired
	private RabbitTemplate rabbitJsonTemplate
	@Autowired
	private RestTemplate httpTemplate // by name
	@Autowired
	OwnerRepository ownerRepository

	@Value('${local.server.port}')
	private int port
	@Value('${spring.application.name}')
	private String applicationName
	@Value('${mkitchenstore.test.consumeMessages}')
	private Boolean consumeMessages

	Traverson traverson

	@Before
	public void setUp(){
		(applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
			rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
		}
	}



	@Test
	void create_and_update_and_delete_foodpoint_should_create_messages() {

		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

		def headers = new HttpHeaders()
		headers.setContentType(MediaTypes.HAL_JSON)
		headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

		Map<String,Object> toPost
		(1..DECORATOR).each {
			toPost = [
					name       : "notNullName${new Random().nextInt()}" as String,
					displayName: "notNullDisplayName",
					location   : [latitude   : new Random().nextDouble(), 
						          longitude  : new Random().nextDouble()],
					owner      : traverson.
									follow("owners","search","byId").
									withTemplateParameters([id : ownerRepository.findAll().first().pk]).
									toObject(new ParameterizedTypeReference<Resource<Owner>>() {}).
									getLink("self").
									getHref()
			]


			def created = httpTemplate.exchange(
					traverson.follow("foodpoints").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(toPost, headers),
					new ParameterizedTypeReference<Resource<FoodPoint>>() {}
			)


			assert created
			assert created.getStatusCode() == HttpStatus.CREATED

			// simple update
			FoodPoint responseEntity = created.getBody().getContent()
			responseEntity.displayName += "_UPDATED"

			def updated = httpTemplate.exchange(
					new URI("http://localhost:${port}/foodpoints/${responseEntity.pk}"),
					HttpMethod.PUT,
					new HttpEntity<FoodPoint>(responseEntity, headers),
					new ParameterizedTypeReference<Resource<FoodPoint>>() {}
			)

			assert updated
			assert updated.getStatusCode() == HttpStatus.OK
			
			// delete resource
			def delete = httpTemplate.exchange(
					new URI("http://localhost:${port}/foodpoints/${responseEntity.pk}"),
					HttpMethod.DELETE,
					new HttpEntity<FoodPoint>(responseEntity, headers),
					new ParameterizedTypeReference<Resource<FoodPoint>>() {}
			)
			assert delete
			assert delete.getStatusCode() == HttpStatus.NO_CONTENT

		}


		// assert the messages were sent and we can receive them
		AssertConsumerHelper.what(rabbitJsonTemplate, DECORATOR * NUMBER_OF_EVENTS, consumeMessages)
	}

}
