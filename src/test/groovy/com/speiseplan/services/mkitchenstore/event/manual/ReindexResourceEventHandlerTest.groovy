package com.speiseplan.services.mkitchenstore.event.manual

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Location
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.event.auto.AssertConsumerHelper
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.util.RabbitMQIntegrationTests
import org.junit.Before
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runner.RunWith
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["rabbitmq-local", "test", "hsql", "events", "audit","populate", "no-security","web"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
//@Ignore("needs a running rabbitmq to be tested")
@Category(RabbitMQIntegrationTests.class)
class ReindexResourceEventHandlerTest {

	@Autowired
	private EmbeddedWebApplicationContext server
	
	@Autowired
	private ApplicationContext applicationContext

	@Autowired
	private MenuRepository menuRepository

	@Autowired
	RabbitTemplate rabbitJsonTemplate

	@Autowired
	private RabbitAdmin rabbitAdmin

	@Autowired
	private RestTemplate httpTemplate // by name

	@Value('${local.server.port}')
	int port

	@Value('${mkitchenstore.test.consumeMessages}')
	private Boolean consumeMessages
	
	@Value('${mkitchenstore.api.resources.incoming.reindex.menu}')
	String reindexMenu

	Traverson traverson

	@Before
	public void setUp(){
		(applicationContext.getBean("allQueues") as List).forEach { Queue queue ->
			rabbitAdmin.purgeQueue(queue.name, Boolean.TRUE)
		}
	}

	@Test
	void server_should_be_running() {
		assert server
		assert applicationContext
		assert port
	}

	@Test
	void reindex_menu_should_create_message(){

		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

		def headers = new HttpHeaders()

		headers.setContentType(MediaType.APPLICATION_JSON)
		headers.setAccept([MediaType.APPLICATION_JSON].asImmutable())

		int decorator = 1
		int numberEvents = 1

		(1..decorator).each {

			def url = UriComponentsBuilder.newInstance().
						scheme("http").
						host("localhost").
						port(port).
						pathSegment(reindexMenu).
						buildAndExpand([
							id: menuRepository.findAll().first().pk
						]).
						toUri().
						toString()
			println url
			def reindex = httpTemplate.exchange(
					url,
					HttpMethod.PUT,
					new HttpEntity(headers),
					new ParameterizedTypeReference<Resource<?>>() {}
					)

			assert reindex

		}

		// assert the messages were sent and we can receive them
		AssertConsumerHelper.what(rabbitJsonTemplate, decorator * numberEvents, consumeMessages)
	}

}
