package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Owner
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test","hsql","populate", "audit"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class OwnerRepositoryTest {

	@Autowired
	ApplicationContext applicationContext
	@Autowired
	OwnerRepository ownerRepository

	Collection<Owner> owners = []
	
	@Before
	void setUp() {
		owners = ownerRepository.findAll()
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		assert applicationContext
		assert ownerRepository
	}

	@Test
	void findAll_should_find_stored_entities() {
		def result = ownerRepository.findAll()
		
		assert result.size() > 0
		assert result.size() == owners.size()
	}
	
	@Test
	void findByPk_should_find_required_entity() {
		Owner owner = owners[new Random().nextInt(owners.size()-1)]
		
		def result = ownerRepository.findByPk(owner.pk)
		
		assert result
		assert result == owner
	}
	
	@Test
	void save_should_persist_entities() {
		def result = ownerRepository.save(new Owner(identifier: 'newguy'))
		assert result
		
		def all = ownerRepository.findAll()
		assert all.size() > 0
		assert all.size() == owners.size() + 1
	}
	
	@Test
	void remove_should_drop_a_entity() {
		
		def result = ownerRepository.save(new Owner(identifier: 'newguy'))
		assert result && result.pk
		def all = ownerRepository.findAll()
		assert all.size() > 0
		assert all.size() == owners.size() + 1
		
		ownerRepository.delete( result.pk )
		
		all = ownerRepository.findAll()
		assert all.size() > 0
		assert all.size() == owners.size()
	}
	
	@Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
	void remove_should_not_be_allowed() {
		ownerRepository.delete( owners[new Random().nextInt(owners.size()-1)].pk )
	}
	
	@Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
	void update_should_not_be_allowed() {
		
		Owner owner = owners[new Random().nextInt(owners.size()-1)]
		owner.identifier = 'another@other.com'
		def result = ownerRepository.save(owner)
	}
}
