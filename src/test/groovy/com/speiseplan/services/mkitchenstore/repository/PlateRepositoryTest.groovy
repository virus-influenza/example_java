package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Plate
import com.speiseplan.services.mkitchenstore.domain.types.Plates
import com.speiseplan.services.mkitchenstore.util.FixtureOwner;
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

/**
 * check /resources/data/foodPoints.json for details about testing data
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test","hsql","populate", "audit"])
class PlateRepositoryTest {

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private PlateRepository plateRepository
    @Autowired
    private MenuRepository menuRepository

    @Autowired
    private OwnerRepository ownerRepository

    private Collection<Menu> menus = []
    private Collection<Plate> plates = []

    @Before
    void setUp() {
        menus  = menuRepository.findAll()
        plates = plateRepository.findAll()
    }

    @After
    void tearDown(){
    }

    @Test
    void assert_context_and_repository() {
        assert applicationContext
        assert plateRepository
    }

    @Test
    void findAll_should_find_stored_entities() {
        def result = plateRepository.findAll()
        // http://domain:number/plates

        assert result.size() == plates.size()
    }

    @Test
    void findAll_sorted_should_find_stored_entities() {
        def result = plateRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name")))
        // http://domain:number/plates?sort=name,ASC

        assert result.size() == plates.size()

        assert result.first() == plates.sort{ it.name }.first()
    }

    @Test
    void findAll_paged_should_find_stored_entities() {
        def result = plateRepository.findAll(new PageRequest(0,1))
        // http://domain:number/plates?page=0,size=1

        assert result.getSize() == 1
    }

    @Test
    void findByOwnerAndNameAndType_should_find_stored_entities() {
        Plate target = plates[ new Random().nextInt( plates.size() )]

        Plate found = plateRepository.findByOwnerIdentifierAndNameAndOfferType(
                target.owner.identifier,
                target.name,
                target.offerType
        )

        assert found
        assert found == target
    }


    @Test
    void searchableById_should_find_stored_entities() {

        Plate target = plates[ new Random().nextInt( plates.size() )]

        Plate found = plateRepository.findByPk(target.pk)

        assert found
        assert found == target
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByMenusPk_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]

        def result = plateRepository.findByOfferCollectionsPk(parent.pk, null) // no pageable

        assert result
        result.getContent().containsAll( parent.offers )
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByFoodPointsOwnerAndFoodPointsNameAndFoodPointsLatitudeAndFoodPointsLongitude_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]

        def result = plateRepository.findByOwnerIdentifierAndOfferCollectionsName(
                parent.owner.identifier,
                parent.name,
        null) // no pageable

        assert result
        assert result.getContent().containsAll( parent.offers )// LazyLoadingException
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByOwner_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]

        def result = plateRepository.findByOwnerIdentifier(
                parent.owner.identifier,
        null) // no pageable

        assert result
    }


    @Test
    void save_should_update_entity() {
        def fields = [
                name     : "notNullName",
                displayName: "notNullName",
                offerType     : Plates.MAIN,
                owner : ownerRepository.save( FixtureOwner.getOne() )
        ]

        Plate result = plateRepository.save(new Plate(fields))

        assert result
        assert result.pk // assert save in database
        assert result.name        == fields.name
        assert result.created
        assert result.modified
    }

    @Test
    @Transactional
    @Rollback
    void update_should_update_entity() {
        def target = plates[new Random().nextInt( plates.size() )]

        target.displayName = "updated name"

        def updated = plateRepository.save(target)

        assert updated
        assert updated.displayName == "updated name"
    }

    @Test
    @Transactional
    @Rollback
    void delete_by_pk_should_delete_entity() {

        int index = new Random().nextInt(plates.size())

        Plate toDelete = plateRepository.findOne(plates[index].pk)
        assert toDelete

        plateRepository.delete(toDelete.pk)
        assert !plateRepository.findOne(plates[index].pk)

    }

}
