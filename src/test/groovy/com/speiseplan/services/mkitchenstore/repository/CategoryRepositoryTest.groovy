package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Category
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql", "populate", "audit"])
class CategoryRepositoryTest {

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private FoodCategoryRepository foodCategoryRepository

    private List<Category> foodCategories = []

    @Before
    void setUp(){
        foodCategories = foodCategoryRepository.findAll()

        assert foodCategories
    }


    @After
    void tearDown(){
    }

    @Test
    void findAll_should_find_stored_entities(){
        assert foodCategories == foodCategoryRepository.findAll()
        assert foodCategories == foodCategoryRepository.findAll(new PageRequest(0, foodCategories.size() )).getContent()

        int pointer = new Random().nextInt( foodCategories.size() )
        // size vs zero based arrays, these are not the droids you are looking for
        int safePointer = (pointer == 0) ? 0 :pointer - 1
        assert foodCategories.get(pointer) == foodCategoryRepository.findAll(new PageRequest(pointer,1)).getContent().first()
    }

    @Test
    @Transactional
    @Rollback
    void update_should_update_entity() {
        Category toUpdate = foodCategoryRepository.findOne(foodCategories[0].pk)
        assert toUpdate

        toUpdate.description = "tasty food 100% cruelty"

        assert "tasty food 100% cruelty" == foodCategoryRepository.save(toUpdate).description
    }

    @Test
    @Transactional
    @Rollback
    void delete_byId_should_delete_entity() {
        Category toDelete = foodCategoryRepository.findOne(foodCategories[0].pk)
        assert toDelete

        foodCategoryRepository.delete(toDelete.pk)
        assert !foodCategoryRepository.findOne(foodCategories[0].pk)
    }

    @Test
    void findByType_should_return_entities() {

        Categories CATEGORY = Categories.values()[ new Random().nextInt( Categories.values().size() ) ]

        Page<Category> result = foodCategoryRepository.findByOfferCategory(CATEGORY , null) // no pagination

        assert result
        result.getContent().each { assert CATEGORY.equals( it.offerCategory ) }
    }

    @Test
    void findByNameAndType_should_return_entity() {
        // unique name and type
        // @UniqueConstraint(name="category_unique_natural_key", columnNames = ["name", "type"]
        Category target = foodCategories[ new Random().nextInt( foodCategories.size() ) ]

        Category result = foodCategoryRepository.findByNameAndOfferCategory(
                target.name,
                target.offerCategory
        )

        assert result
        assert result == target
    }
}
