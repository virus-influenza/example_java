package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.util.FixtureOwner;

import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

/**
 * check /resources/data/menus.json for details about testing data
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql", "populate", "audit"])
class MenuRepositoryTest {

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private MenuRepository menuRepository
    @Autowired
    private FoodPointRepository foodPointRepository

    @Autowired
    private OwnerRepository ownerRepository

    private Collection<FoodPoint> foodPoints
    private Collection<Menu> menus

    @Before
    void setUp() {
        foodPoints = foodPointRepository.findAll()
        menus = menuRepository.findAll()
    }

    @After
    void tearDown() {
    }

    @Test
    void assert_context_and_repository() {
        assert applicationContext
        assert menuRepository
    }

    @Test
    void findAll_should_find_stored_entities() {
        def result = menuRepository.findAll()
        // http://domain:number/menus?sort=name,ASC

        assert result.size() == menus.size()
    }

    @Test
    void findAll_sorted_should_find_stored_entities() {
        def result = menuRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name")))
        // http://domain:number/menus?sort=name,ASC

        assert result.size() == menus.size()

        assert result.first() == menus.sort { it.name }.first()
    }

    @Test
    void findAll_paged_should_find_stored_entities() {
        def result = menuRepository.findAll(new PageRequest(0, 1))
        // http://domain:number/menus?page=0,size=1

        assert result.getSize() == 1
    }

    @Test
    void findByPk_should_find_stored_entity() {
        Menu target = menus[new Random().nextInt(menus.size())]

        Menu found = menuRepository.findByPk(target.pk)

        assert found
        assert found == target
    }

    @Test
    void findByOwnerIdentifierAndName_should_find_stored_entities() {
        Menu target = menus[new Random().nextInt(menus.size())]

        Menu found = menuRepository.findByOwnerIdentifierAndName(target.owner.identifier,target.name)

        assert found
        assert found == target
    }

    @Test
    void findByFoodPointsId_should_find_stored_entities() {
        def parent = foodPoints[0]

        def result = menuRepository.findByOriginPk(parent.pk, new PageRequest(0,20) )

        assert result
        assert result.getContent()
    }

    @Test
    void findByOwner_should_find_stored_entities() {
        def parent = foodPoints[0]

        def result = menuRepository.findByOriginNaturalKey(parent.owner.identifier, parent.name, new PageRequest(0,20)) // no pageable

        assert result

        assert result.content
        //assert result.content.containsAll( parent.offerCollections ) // foodPoint.offerCollections is Lazy, but @Transactional makes the trick
    }


    @Test
    void save_should_update_entity() {
        def fields = [
                name       : "notNullName",
                displayName: "notNullName",
                owner      : ownerRepository.save(new Owner(identifier:"notNullOwner")),
        ]

        Menu result = menuRepository.save(new Menu(fields))

        assert result
        assert result.pk // assert save in database
        assert result.name == fields.name
        assert result.owner.identifier == fields.owner.identifier

        // assert Auditable
        assert result.created
        assert result.modified
    }

    @Test
    @Transactional
    @Rollback
    void update_should_update_entity() {
        def fields = [
                name       : "notNullName",
                displayName : "notNullName",
                owner      : ownerRepository.save( FixtureOwner.getOne() )
        ]

        Menu result = menuRepository.save(new Menu(fields))

        assert result
        assert result.pk // assert save in database
        assert result.name == fields.name
        assert result.owner == fields.owner

        // assert Auditable
        assert result.created
        assert result.modified


        result.displayName = "new"

        Menu updated = menuRepository.save(result)

        assert updated
        assert updated.displayName == "new"
    }

    @Test
    @Transactional
    @Rollback
    void delete_by_pk_should_delete_entity() {

        int index = new Random().nextInt(menus.size())

        Menu toDelete = menuRepository.findOne(menus[index].pk)
        assert toDelete

        menuRepository.delete(toDelete.pk)
        assert !menuRepository.findOne(menus[index].pk)

    }

}
