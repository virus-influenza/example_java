package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Location
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.util.FixtureLocation;
import com.speiseplan.services.mkitchenstore.util.FixtureOwner;

import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

/**
 * check /resources/data/foodpoints.json for details about testing data
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql", "populate", "audit"])
class FoodPointRepositoryTest {

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private FoodPointRepository foodPointRepository

    @Autowired
    private MenuRepository menuRepository

    @Autowired
    private OwnerRepository ownerRepository

    private Collection<FoodPoint> foodPoints = []

    @Before
    void setUp() {
        foodPoints = foodPointRepository.findAll()
    }

    @After
    void tearDown() {
    }

    @Test
    void assert_context_and_repository() {
        assert applicationContext
        assert foodPointRepository
    }

    @Test
    void findAll_should_find_stored_entities() {
        def result = foodPointRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name")))

        assert result.size() == foodPoints.size()
    }

    @Test
    void findByPk_should_find_stored_entity() {
        FoodPoint target = foodPoints[new Random().nextInt(foodPoints.size())]

        def result = foodPointRepository.findByPk(target.pk)

        assert result
        assert result == target
    }

    @Test
    void findByNameAndOwnerAndLatitudeAndLongitude_should_find_stored_entities() {

        int index = new Random().nextInt(foodPoints.size())

        def result = foodPointRepository.findByNameAndOwnerIdentifier(
                foodPoints[index].name,
                foodPoints[index].owner.identifier
        )

        assert result
        assert result == foodPoints[index]
    }

    @Test
    void findByOwner_should_find_stored_entities() {
        int index = new Random().nextInt(foodPoints.size())

        def result = foodPointRepository.findByOwnerIdentifier(foodPoints[index].owner.identifier, new PageRequest(0,20))

        assert result
        result.getContent().each {
            assert it.owner == foodPoints[index].owner
        }
    }


    @Test
    @Transactional
    @Rollback
    void update_should_update_entity() {
        int index = new Random().nextInt(foodPoints.size())

        FoodPoint toUpdate = foodPointRepository.findOne(foodPoints[index].pk)
        assert toUpdate

        toUpdate.location.latitude = 0.666f
        toUpdate.location.longitude = 0.666f

        FoodPoint result = foodPointRepository.save(toUpdate)

        assert result
        assert result.location.latitude == toUpdate.location.latitude
        assert result.location.longitude == toUpdate.location.longitude
    }

    @Test
    @Transactional
    @Rollback
    void save_should_save_auditable_entity() {
        def fields = [
                name       : "notNullName",
                displayName: "notNullName",
                owner      : ownerRepository.findAll().get( new Random().nextInt(ownerRepository.findAll().size()) ),
                location   : FixtureLocation.getOne()
        ]

        FoodPoint result = foodPointRepository.save(new FoodPoint(fields))

        assert result
        assert result.pk // assert save in database
        assert result.name == fields.name
        assert result.owner == fields.owner
        assert result.location == fields.location
		
        // assert Auditable
        assert result.created
        assert result.modified

    }

    @Test
    @Transactional
    @Rollback
    void delete_by_pk_should_delete_entity() {
        int index = new Random().nextInt(foodPoints.size())

        FoodPoint toDelete = foodPointRepository.findOne(foodPoints[index].pk)
        assert toDelete

        foodPointRepository.delete(toDelete.pk)
        assert !foodPointRepository.findOne(foodPoints[index].pk)
    }

   @Test
   @Ignore("menu is not longer related whit foodpoint")
   void findByOfferCollectionsName_should_return_results(){

        Owner owner = ownerRepository.save(
                new Owner(identifier:  "notNullOwner${new Random().nextInt()}"
                )
        )

        FoodPoint foodPoint =  foodPointRepository.save(
                new FoodPoint(
                        name       : "notNullName${new Random().nextInt()}",
                        displayName: "notNullDisplayName",
                        owner      : ownerRepository.findByPk(owner.pk),
                        location   : new Location( latitude:1.666f , longitude:1.666f ),
                )
        )

        Menu menu = menuRepository.save(
                new Menu(
                        name       : "notNullName${new Random().nextInt()}",
                        displayName: "notNullDisplayName",
                        owner      : ownerRepository.findByPk(owner.pk),
                        origins    : [foodPoint]
                )
        )



        assert foodPointRepository.findByOfferCollectionsName(menu.name, new PageRequest(0,20)).first().name == foodPoint.name

    }
}
