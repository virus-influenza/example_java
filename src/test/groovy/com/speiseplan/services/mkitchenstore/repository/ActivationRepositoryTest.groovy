package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Activation
import com.speiseplan.services.mkitchenstore.domain.Menu

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.annotation.Transactional

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

/**
 * check /resources/data/foodPoints.json for details about testing data
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test","hsql","populate", "audit"])
class ActivationRepositoryTest {

    @Autowired
    private ApplicationContext applicationContext

    @Autowired
    private ActivationRepository activationRepository
    @Autowired
    private MenuRepository menuRepository

    private Collection<Menu> menus = []
    private Collection<Activation> activations = []

    def test = new DateTimeFormatterBuilder();

	// activation dates format : @JsonFormat(pattern = "dd/MM/yyyyY")
	DateTimeFormatter activationDatesDateFormat = new DateTimeFormatterBuilder().
																				appendValue(ChronoField.DAY_OF_MONTH).
																				appendLiteral("/").
																				appendValue(ChronoField.MONTH_OF_YEAR).
																				appendLiteral("/").
																				appendValue(ChronoField.YEAR).toFormatter()

    @Before
    void setUp() {
        menus       = menuRepository.findAll()
        activations = activationRepository.findAll()
    }

    @After
    void tearDown(){
    }

    @Test
    void assert_context_and_repository() {
        assert applicationContext
        assert activationRepository
    }

    @Test
    void findAll_should_find_stored_entities() {
        def result = activationRepository.findAll()
        
        assert result.size() == activations.size()
    }

    @Test
	@Transactional
    void findAll_sorted_should_find_stored_entities() {
        def result = activationRepository.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "start")))

        assert result.size() == activations.size()

        assert result.first() == activations.sort{ it.start }.first()
    }

    @Test
    void findAll_paged_should_find_stored_entities() {
        def result = activationRepository.findAll(new PageRequest(0,1))

        assert result.getSize() == 1
    }

    @Test
    @Transactional // activation.menu.owner in assert needs hibernate session to execute the join
    void findAllByMenuFoodPointsOwnerAndStartGreaterThanEqualOrderByStartAsc_should_find_stored_entities() {

        String tester = "lucas.carrillo@gmail.com"

        // every activation of the owner "unit@speiseplan.testers", Menu1, Menu2, Menu3, (Activation1..Activation5)
        def result = activationRepository.findAllByOwnerIdentifierAndStartGreaterThanEqualOrderByStartAsc(
                tester,
                LocalDate.parse("01/01/2016", activationDatesDateFormat),
                new PageRequest(0,20)
        )

        assert result
        assert result.getContent().size() == 5 // every activation of the owner "unit@speiseplan.testers", Menu1, Menu2, Menu3, (Activation1..Activation5)

        assert result.getContent().every {it.start >= LocalDate.parse("01/01/2016", activationDatesDateFormat)}
        assert result.getContent().every { it.owner.identifier == tester }
    }


    @Test
    @Transactional // activation.menu.owner in assert needs hibernate session to execute the join
    void findAllByMenuOwnerAndStartBeforeOrderByStartDesc_should_find_stored_entities() {

        String tester = "lucas.carrillo@gmail.com"
        Activation target = activations[ new Random().nextInt( activations.size() )]
        
        // every activation of the owner "unit@speiseplan.testers", Menu1, Menu2, Menu3, (Activation1..Activation5)
        def result = activationRepository.findAllByOwnerIdentifierAndStartBeforeOrderByStartDesc(
                tester,
                LocalDate.parse("01/01/2020",activationDatesDateFormat), // :)
                new PageRequest(0,20)
        )

        assert result
        assert result.getContent().size() == 5 // every activation of the owner "unit@speiseplan.testers", Menu1, Menu2, Menu3, (Activation1..Activation5)

        assert result.getContent().every {it.start < LocalDate.parse("01/01/2020", activationDatesDateFormat)}
        assert result.getContent().every { it.owner.identifier == tester }
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByMenusPk_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByFoodPointsOwnerAndFoodPointsNameAndFoodPointsLatitudeAndFoodPointsLongitude_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]

        // assert result.getContent().containsAll( parent.activations )// LazyLoadingException
    }

    @Test
    @Transactional // avoid lazy loading exception, hibernate session bounded to the transactional context 8)
    void findByOwner_should_find_stored_entities() {
        def parent = menus[ new Random().nextInt( menus.size() )]
    }


	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findAllByMenuPkAndStartGreaterThanEqualOrderByStartAsc_should_find_stored_entities() {
		
		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findAllByOfferCollectionPkAndStartGreaterThanEqualOrderByStartAsc(
				tester,
				LocalDate.parse("01/01/2016",activationDatesDateFormat),
				new PageRequest(0,20)
		)

		
		assert result
		assert result.getContent().size() == 3 // every activation of the  Menu1 (Activation1..Activation3)

		assert result.getContent().every {it.start >= LocalDate.parse("01/01/2016", activationDatesDateFormat)}
		assert result.getContent().every { it.offerCollection.pk == tester }
	}

	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findAllByMenuPkAndStartBeforeOrderByStartDesc_should_find_stored_entities() {

		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findAllByOfferCollectionPkAndStartBeforeOrderByStartDesc(
				tester,
				LocalDate.parse("01/08/2016",activationDatesDateFormat),
				new PageRequest(0,20)
		)

		assert result
		assert result.getContent().size() == 2 // every inactive activation of the  Menu1 (Activation1..Activation2)

		assert result.getContent().every {it.start < LocalDate.parse("01/08/2016", activationDatesDateFormat)}
		assert result.getContent().every { it.offerCollection.pk == tester }
	}
	
	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findByMenuFoodPointsPkAndStartGreaterThanEqualOrderByStartAsc_should_find_stored_entities() {

		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findByOriginPkAndStartGreaterThanEqualOrderByStartAsc(
				tester,
                LocalDate.parse("01/07/2016", activationDatesDateFormat),
				new PageRequest(0,20)
		)

		assert result
		assert result.getContent().size() == 3 // every active activation of the  FoodPoint11 (Activation2..Activation4)

		assert result.getContent().every {it.start >= LocalDate.parse("01/07/2016", activationDatesDateFormat)}
		assert result.getContent().every { it.origin.pk == tester}
	}
	
	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findByMenuFoodPointsPkAndStartBefore_should_find_stored_entities() {

		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findByOriginPkAndStartBefore(
				tester,
				LocalDate.parse("01/08/2016", activationDatesDateFormat),
				new PageRequest(0,20)
		)

		assert result
		assert result.getContent().size() == 3 // every in active activation of the  FoodPoint11 (Activation1,Activation2,Activation4)

		assert result.getContent().every {it.start < LocalDate.parse("01/08/2016", activationDatesDateFormat)}
		assert result.getContent().every { it.origin.pk == tester}
	}

	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findAllByMenuPkOrderByStart_should_find_stored_entities() {

		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findAllByOfferCollectionPkOrderByStart(
				tester,
				new PageRequest(0,20)
		)

		assert result
		assert result.getContent().size() == 3 // every activation of the  Menu1 (Activation1,Activation2,Activation3)
		
		assert result.getContent().every { it.offerCollection.pk == tester}
	}
	
	@Test
	@Transactional // activation.menu.owner in assert needs hibernate session to execute the join
	void findByMenuFoodPointsPk_should_find_stored_entities() {

		Long tester = 1L
		Activation target = activations[ new Random().nextInt( activations.size() )]

		def result = activationRepository.findByOriginPk(
				tester,
				new PageRequest(0,20)
		)

		assert result
		assert result.getContent().size() == 4 // every activation of the  FoodPoint1 (Activation1..Activation4)
		
		assert result.getContent().every { it.origin.pk == tester}
	}
	
    @Test
    void save_should_update_entity() {
    }

    @Test
    @Transactional
    @Rollback
    void update_should_update_entity() {
        def target = activations[new Random().nextInt( activations.size() )]
    }

    @Test
    @Transactional
    @Rollback
    void delete_by_pk_should_delete_entity() {

        int index = new Random().nextInt(activations.size())

        Activation toDelete = activationRepository.findOne(activations[index].pk)
        assert toDelete

        activationRepository.delete(toDelete.pk)
        assert !activationRepository.findOne(activations[index].pk)

    }

}
