package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Location
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test","hsql","populate", "audit"])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class LocationRepositoryTest {

	@Autowired
	ApplicationContext applicationContext
	@Autowired
	LocationRepository locationRepository

	Collection<Location> locations = []
	
	@Before
	void setUp() {
		locations = locationRepository.findAll()
	}

	@After
	void tearDown(){
	}

	@Test
	void assert_context_and_repository() {
		assert applicationContext
		assert locationRepository
	}

	@Test
	void findAll_should_find_stored_entities() {
		def result = locationRepository.findAll()
		
		assert result.size() > 0
		assert result.size() == locations.size()
	}
	
	@Test
	void findByPk_should_find_required_entity() {
		Location location = locations[new Random().nextInt(locations.size()-1)]
		
		def result = locationRepository.findByPk(location.pk)
		
		assert result
		assert result == location
	}
	
	@Test
	void save_should_persist_entities() {
		def result = locationRepository.save(new Location(
													description: 'some place',
													city: 'mycity',
													address: 'myaddress',
													zipCode: '00001',
													latitude: 1.23,
													longitude: 5.636,
													))
		assert result
		
		def all = locationRepository.findAll()
		assert all.size() > 0
		assert all.size() == locations.size() + 1
	}
	
	void update_should_works_fine() {
		
		Location location = locations[new Random().nextInt(locations.size()-1)]
		location.description = 'some place changed'
		location.city = 'mycity changed'
		location.address = 'myaddress changed'
		location.zipCode = '00001'
		location.latitude = 1.23
		location.longitude = 5.636
		locationRepository.save(location)
		
		def result = locationRepository.findByPk(location.pk)
		assert result 			  == location
		assert result.description == location.description
		assert result.city 		  == location.city
		assert result.address 	  == location.address
		assert result.zipCode  	  == location.zipCode
		assert result.latitude    == location.latitude
		assert result.longitude   == location.longitude
	}
	
	@Test
	void remove_should_drop_a_entity() {
		
		def result = locationRepository.save(new Location(
				description: 'some place',
				city: 'mycity',
				address: 'myaddress',
				zipCode: '00001',
				latitude: 1.23,
				longitude: 5.636,
				))
		assert result

		def all = locationRepository.findAll()
		assert all.size() > 0
		assert all.size() == locations.size() + 1
		
		//delete entity
		locationRepository.delete(result.pk)
		all = locationRepository.findAll()
		assert all.size() > 0
		assert all.size() == locations.size()
	}
	
	@Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
	void remove_related_entity_should_not_be_allowed() {
		locationRepository.delete( locationRepository.delete( locations[new Random().nextInt(locations.size()-1)].pk ) )
	}
}
