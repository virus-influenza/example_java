package com.speiseplan.services.mkitchenstore.util

import com.speiseplan.services.mkitchenstore.domain.Owner;

class FixtureOwner {

	static Owner getOne(){
		return new Owner([
                                identifier   : "unit${new Random().nextInt()}@speiseplan.testers"
                        ])
	}
}
