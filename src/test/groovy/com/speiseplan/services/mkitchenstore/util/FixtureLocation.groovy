package com.speiseplan.services.mkitchenstore.util

import com.speiseplan.services.mkitchenstore.domain.Location;

class FixtureLocation {

    static Location getOne() {
        Double random = new Random().nextInt();


        return new Location([
                description: "description-${random}",
                city       : "test-city-${random}",
                zipCode    : new Random().nextInt(99999),
                latitude   : new Random().nextDouble(),
                longitude  : new Random().nextDouble()
        ])
    }
}
