package com.speiseplan.services.mkitchenstore.controller.event

import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.model.request.OfferCollectionReindexRequest
import com.speiseplan.services.mkitchenstore.model.response.OfferCollectionReindexResponse
import com.speiseplan.services.mkitchenstore.service.event.ManualEventTriggerService
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.mockito.Mockito.when

@RunWith(MockitoJUnitRunner.class)
class OfferCollectionManualEventControllerUnitTest {

	@Mock
	ManualEventTriggerService<OfferCollectionReindexRequest, OfferCollectionReindexResponse> manualEventTriggerService

	@InjectMocks
	OfferCollectionManualEventController offerCollectionManualEventController = new OfferCollectionManualEventController()

	@Test
	void assert_offer_collection_manual_reindex_call_works_fine(){

		Menu fromDatabase = new Menu(
				pk: 2,
				name: "myMenu",
				owner: new Owner(identifier: "myOwner")
		)

		OfferCollectionReindexRequest request = new OfferCollectionReindexRequest(pk: fromDatabase.pk)
		OfferCollectionReindexResponse response = new OfferCollectionReindexResponse(pk: fromDatabase.pk)

		when(manualEventTriggerService.triggerSingleResourceReindexByPk(fromDatabase.pk)).thenReturn(response)


		def httpResponse = offerCollectionManualEventController.reindex(fromDatabase.pk)

		assert httpResponse == null
	}
}
