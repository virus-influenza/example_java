package com.speiseplan.services.mkitchenstore.service.event

import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import com.speiseplan.services.mkitchenstore.event.manual.ReindexResourceEventHandler
import com.speiseplan.services.mkitchenstore.model.request.OfferCollectionReindexRequest
import com.speiseplan.services.mkitchenstore.model.response.OfferCollectionReindexResponse
import com.speiseplan.services.mkitchenstore.repository.MenuRepository

import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

import static org.hamcrest.CoreMatchers.containsString

import static org.mockito.Mockito.*


@RunWith(MockitoJUnitRunner.class)
class OfferCollectionManualEventTriggerServiceUnitTest {


	@Mock
	MenuRepository menuRepository

	@Mock
	ReindexResourceEventHandler<Menu> menuReindexResourceEventHandler

	@InjectMocks
	ManualEventTriggerService<Long, OfferCollectionReindexResponse> offerCollectionManualEventTriggerService = new OfferCollectionManualEventTriggerService()

	@Rule
	public ExpectedException thrown = ExpectedException.none()


	@Test
	void assert_total_menu_is_send() {

		Menu fromDatabase = new Menu(
				pk: 2,
				name: "myMenu",
				owner: new Owner(identifier: "myOwner")
		)

		Long request = fromDatabase.pk
		OfferCollectionReindexResponse response = new OfferCollectionReindexResponse(pk: fromDatabase.pk)

		when(menuRepository.findByPk(request)).thenReturn(fromDatabase)
		//https://stackoverflow.com/questions/9375356/bug-in-mockito-with-grails-groovy
		//doNothing().when(menuReindexResourceEventHandler).handleEvent(fromDatabase,ResourceEvents.RESPAWN)
		doReturn(ReindexResourceEventHandler.metaClass).when(menuReindexResourceEventHandler).handleEvent(fromDatabase,ResourceEvents.RESPAWN)

		OfferCollectionReindexResponse result = offerCollectionManualEventTriggerService.triggerSingleResourceReindexByPk(request)

		assert result
		assert result.pk == response.pk
	}

	@Test
	void assert_null_request_object_throw_exception() {

		//test type
		thrown.expect(IllegalArgumentException.class);

		//test message
		thrown.expectMessage(containsString("Id not valid"));

		offerCollectionManualEventTriggerService.triggerSingleResourceReindexByPk(null)
	}
	
	@Test
	void assert_imposible_pk_throw_not_found_exception() {

		//test type
		thrown.expect(IllegalArgumentException.class);

		//test message
		thrown.expectMessage(containsString("resource not found"));

		// request
		Long request = 666

		// expectations
		when(menuRepository.findByPk(request)).thenReturn(null)

		// call for exception
		offerCollectionManualEventTriggerService.triggerSingleResourceReindexByPk(request)
	}
}
