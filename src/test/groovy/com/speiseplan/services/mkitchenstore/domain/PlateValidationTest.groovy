package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.domain.types.Plates
import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.repository.PlateRepository
import com.speiseplan.services.mkitchenstore.util.FixtureOwner;

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class PlateValidationTest {

    @Autowired
    PlateRepository plateRepository

    @Autowired
    OwnerRepository ownerRepository

    @Test
    void assert_repository() {
        assert plateRepository
    }

    @Test(expected = ConstraintViolationException)
    void assert_empty_resource_failed_validation() {
        plateRepository.save(
                new Menu([:])
        )
    }

    @Test(expected = ConstraintViolationException)
    void assert_notNull_fields_failed_validation() {
        def fields = [
                name       : "notNullName${new Random().nextInt()}",
                owner      : ownerRepository.save(FixtureOwner.getOne()) ,
                displayName: "notNullDisplayName",
                offerType       : Plates.MAIN
        ]

        // set one of the not null fields to null, check validation failing, profits everywhere
        fields.put(fields.keySet().toArray()[new Random().nextInt(fields.keySet().size())] as String, null)

        plateRepository.save(
                new Plate(fields)
        )
    }


    @Test
    void assert_validation_passed() {

        plateRepository.save(
                new Plate([
                        name       : "notNullName${new Random().nextInt()}",
                        displayName: "notNullDisplayName",
                        owner      : ownerRepository.save(FixtureOwner.getOne()) ,
                        offerType       : Plates.MAIN
                ])
        )
    }
}
