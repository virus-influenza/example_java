package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.repository.ActivationRepository
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureOwner
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException
import java.time.LocalDate

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class ActivationValidationTest {

	@Autowired
	ActivationRepository activationRepository

	@Autowired
	OwnerRepository ownerRepository
	
	@Autowired
	MenuRepository menuRepository
	
	@Test
	void assert_repository() {
		assert activationRepository
		assert ownerRepository
	}
	
	@Test(expected = ConstraintViolationException)
	void assert_empty_resource_failed_validation() {
		activationRepository.save(
				new Activation([:])
		)
	}

	@Test(expected = ConstraintViolationException)
	void assert_notNull_fields_failed_validation() {
		
		Owner owner = ownerRepository.save(FixtureOwner.getOne())
		Menu menu = menuRepository.save(
						                new Menu(name       : "notNullName${new Random().nextInt()}",
										         displayName: "notNullDisplayName",
										         owner      : owner
												)
										)
		def fields = [
			owner      		: owner,
			offerCollection : menu,
			start      		: LocalDate.now(),
			end        		: LocalDate.now().plusMonths(5)
		]
		// set one of the not null fields to null, check validation failing, profits everywhere
		fields.put(fields.keySet().toArray()[new Random().nextInt(fields.keySet().size())] as String, null)

		activationRepository.save(
				new Activation(fields)
		)
	}

	@Test
	void assert_validation_passed() {
		Owner owner = ownerRepository.save(FixtureOwner.getOne())
		Menu menu = menuRepository.save(
						                new Menu(name       : "notNullName${new Random().nextInt()}",
										         displayName: "notNullDisplayName",
										         owner      : owner
												)
										)
		def fields = [
			owner      		: owner,
			offerCollection : menu,
			start      		: LocalDate.now(),
			end        		: LocalDate.now().plusMonths(5)
		]

		def result = activationRepository.save(new Activation(fields))
		assert result && result.pk
	}

}
