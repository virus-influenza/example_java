package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureOwner
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class MenuValidationTest {

    @Autowired
    MenuRepository menuRepository

    @Autowired
    OwnerRepository ownerRepository

    Owner owner

    @Test
    void assert_repository() {
        assert menuRepository
    }

    @Before
    void setUp(){

        owner = ownerRepository.save(new Owner(identifier:  "notNullOwner${new Random().nextInt()}"))

        assert owner
        assert owner.pk
    }

    @Test(expected = ConstraintViolationException)
    void assert_empty_resource_failed_validation() {
        menuRepository.save(
                new Menu([:])
        )
    }

    @Test(expected = ConstraintViolationException)
    void assert_notNull_fields_failed_validation() {
        def fields = [
                name       : "notNullName${new Random().nextInt()}",
                displayName: "notNullDisplayName",
                owner      : ownerRepository.findByPk(owner.pk) ,
        ]

        // set one of the not null fields to null, check validation failing, profits everywhere
        fields.put(fields.keySet().toArray()[new Random().nextInt(fields.keySet().size())] as String, null)

        menuRepository.save(
                new Menu(fields)
        )
    }


    @Test
    void assert_validation_passed() {

        menuRepository.save(
                new Menu([
                        name       : "notNullName${new Random().nextInt()}",
                        displayName: "notNullDisplayName",
                        owner      : ownerRepository.findByPk(owner.pk)
                ])
        )
    }
}
