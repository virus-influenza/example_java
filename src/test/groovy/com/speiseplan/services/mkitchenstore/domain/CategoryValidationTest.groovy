package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import com.speiseplan.services.mkitchenstore.repository.FoodCategoryRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class CategoryValidationTest {

	@Autowired
	FoodCategoryRepository categoryRepository

	@Test
	void assert_repository() {
		assert categoryRepository
	}
	
	@Test(expected = ConstraintViolationException)
	void assert_empty_resource_failed_validation() {
		categoryRepository.save(
				new Category([:])
		)
	}

	
	@Test(expected = ConstraintViolationException)
	void assert_notNull_fields_failed_validation() {
		
		def fields = [
				name   		  : 'categoryName',
				displayName   : 'my-displayName',
				offerCategory : Categories.MAIN
				]

		// set one of the not null fields to null, check validation failing, profits everywhere
		fields.put(fields.keySet().toArray()[new Random().nextInt(fields.keySet().size())] as String, null)

		categoryRepository.save(
				new Category(fields)
		)
	}

	@Test
	void assert_validation_passed() {
		
		def fields = [
				name   		  : 'categoryName',
				displayName   : 'my-displayName',
				offerCategory : Categories.MAIN
				]

		categoryRepository.save(
				new Category(fields)
		)
	}
}
