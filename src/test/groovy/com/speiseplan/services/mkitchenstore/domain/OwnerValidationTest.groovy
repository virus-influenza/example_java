package com.speiseplan.services.mkitchenstore.domain

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.speiseplan.services.mkitchenstore.repository.OwnerRepository;
import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class OwnerValidationTest {

	@Autowired
	OwnerRepository ownerRepository

	@Test
	void assert_repository() {
		assert ownerRepository
	}

	@Test(expected = ConstraintViolationException)
	void assert_empty_resource_failed_validation() {
		ownerRepository.save(
				new Owner([:])
		)
	}

	@Test
	void assert_validation_passed() {

		ownerRepository.save(
				new Owner(identifier: "notNullName")
		)
	}
}
