package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.config.Application
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.speiseplan.services.mkitchenstore.repository.LocationRepository;
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
@Ignore("there are not mandatory fields yet")
class LocationValidationTest {
	
	@Autowired
	LocationRepository locationRepository

	@Test
	void assert_repository() {
		assert locationRepository
	}
	
}
