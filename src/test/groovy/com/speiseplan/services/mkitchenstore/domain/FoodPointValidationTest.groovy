package com.speiseplan.services.mkitchenstore.domain

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureOwner
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration

import javax.validation.ConstraintViolationException

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@TransactionConfiguration
@ActiveProfiles(["test", "hsql-local"])
class FoodPointValidationTest {

    @Autowired
    FoodPointRepository foodPointRepository

    @Autowired
    OwnerRepository ownerRepository

    Owner owner


    @Before
    void setUp(){

        owner = ownerRepository.save(new Owner(identifier:  "notNullOwner${new Random().nextInt()}"))

        assert owner
        assert owner.pk
    }

    @Test
    void assert_repository() {
        assert foodPointRepository
    }

    @Test(expected = ConstraintViolationException)
    void assert_empty_resource_failed_validation() {
        foodPointRepository.save(
                new FoodPoint([:])
        )
    }

    @Test(expected = ConstraintViolationException)
    void assert_notNull_fields_failed_validation() {

        // not null fields
        def fields = [
                name       : "notNullName${new Random().nextInt()}",
                displayName: "notNullDisplayName",
                owner      : ownerRepository.findByPk(owner.pk),
                location   : new Location(latitude:1.666f , longitude:1.666f )

        ]

        // set one of the not null fields to null, check validation failing, profits everywhere
        fields.put(fields.keySet().toArray()[new Random().nextInt(fields.keySet().size())] as String, null)

        foodPointRepository.save(
                new FoodPoint(fields)
        )
    }


    @Test
    void assert_validation_passed() {

        foodPointRepository.save(
                new FoodPoint([
                        name       : "notNullName${new Random().nextInt()}",
                        displayName: "notNullDisplayName",
                        owner      : ownerRepository.findByPk(owner.pk) ,
                        location   : new Location( latitude:1.666f , longitude:1.666f ),
                ])
        )
    }
}
