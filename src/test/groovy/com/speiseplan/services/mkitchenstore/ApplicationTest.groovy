package com.speiseplan.services.mkitchenstore

import com.speiseplan.services.mkitchenstore.config.Application
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = Application
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test","hsql","populate", "audit"])
class ApplicationTest {

    @Autowired
    private EmbeddedWebApplicationContext server

    @Autowired
    private EmbeddedServletContainerFactory bean

    @Autowired
    private ApplicationContext applicationContext

    @Value('${local.server.port}')
    int port

    @Test
    void server_should_be_running(){
        assert server
        assert applicationContext
        assert port
    }
}
