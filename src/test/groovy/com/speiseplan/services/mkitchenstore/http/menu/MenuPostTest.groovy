package com.speiseplan.services.mkitchenstore.http.menu

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.*
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import com.speiseplan.services.mkitchenstore.domain.types.Plates
import com.speiseplan.services.mkitchenstore.repository.ActivationRepository
import com.speiseplan.services.mkitchenstore.repository.FoodCategoryRepository
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

import java.time.LocalDate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = [
                Application
        ]
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "populate-prod","hsql-local", "no-security"])
class MenuPostTest {

    @Autowired
    @Qualifier("httpTemplate")
    RestTemplate restTemplate

    @Value('${local.server.port}')
    int port

    private Traverson traverson

    private HttpHeaders halJson
    private HttpHeaders textUriList
    
    private ResponseEntity<Resource<Owner>> ownerPostResponse
   // private ResponseEntity<Resource<FoodPoint>> foodPointPostResponse

    private Collection<ResponseEntity<Resource<Plate>>> platePostResponseCollection = []
    private Collection<ResponseEntity<Resource<Activation>>> activationPostResponseCollection = []
    private PagedResources<Resource<Activation>> activationSearchResponse
    private PagedResources<Resource<Category>> categorySearchResponse


    @Autowired
    FoodCategoryRepository foodCategoryRepository

    @Autowired
    ActivationRepository activationRepositorypus


    @Before
    void setUp() {

        // traverson hateoas client
        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)
        traverson.setRestOperations(restTemplate)

        // headers
        // application/hal+json
        halJson = new HttpHeaders()
        halJson.setContentType(MediaTypes.HAL_JSON)
        halJson.setAccept([MediaTypes.HAL_JSON].asImmutable())

        // text/uri-list
        textUriList = new HttpHeaders()
        textUriList.setContentType(RestMediaTypes.TEXT_URI_LIST)
        textUriList.setAccept([MediaTypes.HAL_JSON].asImmutable())

        // 1) create an owner ( owner )
        Map<String,Object> owner = [
                identifier: "myOwner${new Random().nextInt()}" as String
        ]

        ownerPostResponse = restTemplate.exchange(
                traverson.follow("owners").asLink().getHref(),
                HttpMethod.POST,
                new HttpEntity(owner, halJson),
                new ParameterizedTypeReference<Resource<Owner>>() {}
        )

        assert ownerPostResponse
        assert ownerPostResponse.statusCode == HttpStatus.CREATED

        // 2) create an origin ( foodPoint )
//        Map<String,Object> foodPoint = [
//                name       : "notNullName${new Random().nextInt()}" as String,
//                displayName: "notNullDisplayName",
//                owner      : ownerPostResponse.getBody().getLink("self").getHref(),
//                location   : new Location(latitude:19.432608f , longitude:-99.133208f )
//        ]

       /* foodPointPostResponse = restTemplate.exchange(
                traverson.follow("foodpoints").asLink().getHref(),
                HttpMethod.POST,
                new HttpEntity(foodPoint, halJson),
                new ParameterizedTypeReference<Resource<FoodPoint>>() {}
        )

        assert foodPointPostResponse
        assert foodPointPostResponse.statusCode == HttpStatus.CREATED*/

        // 3) create offers ( plate )

        def plates = (0..5).collect {
            Map<String,Object> plate = [
                    name       : "notNullName${new Random().nextInt()}" as String,
                    displayName: "notNullDisplayName",
                    owner      : ownerPostResponse.getBody().getLink("self").getHref(),
                    offerType  : Plates.values()[new Random().nextInt(Plates.values().size())]
            ]
            
            
            return plate
        }

        plates.each {

            def platePostResponse = restTemplate.exchange(
                    traverson.follow("plates").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(it, halJson),
                    new ParameterizedTypeReference<Resource<Plate>>() {}
            )

            assert platePostResponse
            assert platePostResponse.statusCode == HttpStatus.CREATED

            platePostResponseCollection.add( platePostResponse )

        }

        // 4) create activations ( activation )

        // Activation is the owner of the relationship, when creating an activation we must have an already
        // stored Menu to associate it with. Check code in post_full_resource test

        
        // 5) categories by populate

        foodCategoryRepository.findAll()

        // 5.1) search by type
        categorySearchResponse = traverson.follow("categories", "search","byType").withTemplateParameters([
                type: Categories.values()[new Random().nextInt(Categories.values().size())]
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Category>>>() {})

        assert categorySearchResponse
        assert categorySearchResponse.getContent()

        // 5.2) find all
        categorySearchResponse = traverson.follow("categories").withTemplateParameters([
                type: Categories.values()[new Random().nextInt(Categories.values().size())]
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Category>>>() {})

        assert categorySearchResponse
        assert categorySearchResponse.getContent()

    }


    @Test
    void post_simple_resource_should_create_entity(){
        
        Map<String,Object> menu = [
                name       : "notNullName${new Random().nextInt()}" as String,
                displayName: "notNullDisplayName",
                owner      : ownerPostResponse.getBody().getLink("self").getHref(),
        ]
        
        
        def menuPostResponse = restTemplate.exchange(
                traverson.follow("menus").asLink().getHref(),
                HttpMethod.POST,
                new HttpEntity(menu, halJson),
                new ParameterizedTypeReference<Resource<Menu>>() {}
        )


        assert menuPostResponse
        assert menuPostResponse.statusCode == HttpStatus.CREATED
    }


    @Test
    void post_full_resource_should_create_entity(){
        
	//	assert activationPostResponseCollection
		
		Map<String,Object> menu = [
                name       : "notNullName${new Random().nextInt()}" as String,
                displayName: "notNullDisplayName",
                owner      : ownerPostResponse.getBody().getLink("self").getHref(),

//                origins    : [ foodPointPostResponse.getBody().getLink("self").getHref() ],
                offers     : platePostResponseCollection.collect{ it.getBody().getLink("self").getHref() },
                activations: activationPostResponseCollection.collect{ it.getBody().getLink("self").getHref() },

                categories : categorySearchResponse.getContent().collect{ it.getLink("self").getHref() }
        ]


        def menuPostResponse = restTemplate.exchange(
                traverson.follow("menus").asLink().getHref(),
                HttpMethod.POST,
                new HttpEntity(menu, halJson),
                new ParameterizedTypeReference<Resource<Menu>>() {}
        )


        assert menuPostResponse
        assert menuPostResponse.statusCode == HttpStatus.CREATED

        assert Boolean.TRUE


        // 1) OWNER
        // navigate using links
        assert restTemplate.exchange(menuPostResponse.getBody().getLink("owner").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<Resource<Owner>>() {} ).getBody().getContent().getIdentifier() == ownerPostResponse.getBody().getContent().getIdentifier()

        // 2) FOODPOINT
        // search food point by unique menu name
//        assert traverson.follow("foodpoints","search","byMenuName").withTemplateParameters([
//                name: menuPostResponse.getBody().getContent().getName()
//        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<FoodPoint>>>() {}).getContent().first().getContent().name == foodPointPostResponse.getBody().getContent().name
        // navigate using links
//        assert restTemplate.exchange(menuPostResponse.getBody().getLink("origins").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<org.springframework.hateoas.Resources<FoodPoint>>() {} ).getBody().getContent().first().name


        // 3) ACTIVATIONS
        // 1) Create
        def activations = (0..5).collect {
            Map<String,Object> activation = [
                    start      : LocalDate.now(),
                    end        : LocalDate.now().plusDays(it),
                    owner      : ownerPostResponse.getBody().getLink("self").getHref(),
                    location   : new Location(latitude:19.432608f , longitude:-99.133208f ),
                    offerCollection: menuPostResponse.getBody().getLink("self").getHref()
            ]

            return activation
        }

        activations.each {

            def activationPostResult = restTemplate.exchange(
                    traverson.follow("activations").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(it, halJson),
                    new ParameterizedTypeReference<Resource<Activation>>() {}
            )

            assert activationPostResult
            assert activationPostResult.statusCode == HttpStatus.CREATED

            activationPostResponseCollection.add(activationPostResult)
        }

        // 2) Test

        // search activation by menu unique name
        assert traverson.follow("activations","search","byMenuName").withTemplateParameters([
                name: menuPostResponse.getBody().getContent().getName()
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {}).getContent().size() == activations.size()

        // navigate using links
        assert restTemplate.exchange(
                menuPostResponse.getBody().getLink("activations").getHref() ,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<org.springframework.hateoas.Resources<Activation>>() {}
        ).getBody().getContent().size() == activations.size()


        // 4) CATEGORIES

        assert restTemplate.exchange(menuPostResponse.getBody().getLink("categories").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<org.springframework.hateoas.Resources<Category>>() {} )


        // 5) PLATES

        // search plates by menu unique name
        assert traverson.follow("plates","search","byMenuNaturalId").withTemplateParameters([
                owner : ownerPostResponse.getBody().getContent().getIdentifier(),
                name: menuPostResponse.getBody().getContent().getName()
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Plate>>>() {}).getContent().size() == platePostResponseCollection.size()


        // navigate using links
        assert restTemplate.exchange(
                menuPostResponse.getBody().getLink("offers").getHref() ,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<org.springframework.hateoas.Resources<Plate>>() {}
        ).getBody().getContent().size() == platePostResponseCollection.size()

    }


    @Test
    void post_simple_resource_and_associate_should_create_entity(){

        Map<String,Object> menu = [
                name       : "notNullName${new Random().nextInt()}" as String,
                displayName: "notNullDisplayName",
                // 0 ) CREATE with owner, not-null
                owner      : ownerPostResponse.getBody().getLink("self").getHref(),
        ]

        def menuPostResponse = restTemplate.exchange(
                traverson.follow("menus").asLink().getHref(),
                HttpMethod.POST,
                new HttpEntity(menu, halJson),
                new ParameterizedTypeReference<Resource<Menu>>() {}
        )

        assert menuPostResponse
        assert menuPostResponse.statusCode == HttpStatus.CREATED

        // 1) UPDATE origin
       /* def associateFoodPointResult = restTemplate.exchange(
                menuPostResponse.getBody().getLink("origins").getHref() ,
                HttpMethod.PUT,
                new HttpEntity<String>(foodPointPostResponse.getBody().getLink("self").getHref(), textUriList),
                new ParameterizedTypeReference<Resource<Owner>>() {}
        )

        assert associateFoodPointResult
        assert associateFoodPointResult.statusCode == HttpStatus.NO_CONTENT*/

        // 2) UPDATE offers
        def associatePlatesResult = restTemplate.exchange(
                menuPostResponse.getBody().getLink("offers").getHref() ,
                HttpMethod.PUT,
                new HttpEntity<String>(platePostResponseCollection.collect{ it.getBody().getLink("self").getHref() }.join(System.getProperty("line.separator")), textUriList),
                new ParameterizedTypeReference<Resource<Owner>>() {}
        )

        assert associatePlatesResult
        assert associatePlatesResult.statusCode == HttpStatus.NO_CONTENT

        // 3) UPDATE category
        def associateCategoriesResult = restTemplate.exchange(
                menuPostResponse.getBody().getLink("categories").getHref() ,
                HttpMethod.PUT,
                new HttpEntity<String>(categorySearchResponse.collect{ it.getLink("self").getHref() }.join(System.getProperty("line.separator")), textUriList),
                new ParameterizedTypeReference<Resource<Category>>() {}
        )


        assert associateCategoriesResult
        assert associateCategoriesResult.statusCode == HttpStatus.NO_CONTENT


        // 4) CREATE activation
        def activations = (0..5).collect {
            Map<String,Object> activation = [
                    start      : LocalDate.now(),
                    end        : LocalDate.now().plusDays(it),
                    owner      : ownerPostResponse.getBody().getLink("self").getHref(),
                    location   : new Location(latitude:19.432608f , longitude:-99.133208f ),
                    // menu in CREATE activation
                    offerCollection: menuPostResponse.getBody().getLink("self").getHref()
            ]

            return activation
        }

        activations.each {

            def activationPostResult = restTemplate.exchange(
                    traverson.follow("activations").asLink().getHref(),
                    HttpMethod.POST,
                    new HttpEntity(it, halJson),
                    new ParameterizedTypeReference<Resource<Activation>>() {}
            )

            assert activationPostResult
            assert activationPostResult.statusCode == HttpStatus.CREATED

            activationPostResponseCollection.add(activationPostResult)
        }



        // TEST

        // 1) OWNER
        // navigate using links
        assert restTemplate.exchange(
                menuPostResponse.getBody().getLink("owner").getHref() ,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Resource<Owner>>() {}
        ).getBody().getContent().getIdentifier() == ownerPostResponse.getBody().getContent().getIdentifier()

        // 2) FOODPOINT
        // search food point by unique menu name
//        assert traverson.follow("foodpoints","search","byMenuName").withTemplateParameters([
//                name: menuPostResponse.getBody().getContent().getName()
//        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<FoodPoint>>>() {}).getContent().first().getContent().name == foodPointPostResponse.getBody().getContent().name
        // navigate using links
//        assert restTemplate.exchange(menuPostResponse.getBody().getLink("origins").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<org.springframework.hateoas.Resources<FoodPoint>>() {} ).getBody().getContent().first().name


        // 3) ACTIVATIONS

        // search activation by menu unique name
        assert traverson.follow("activations","search","byMenuName").withTemplateParameters([
                name: menuPostResponse.getBody().getContent().getName()
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {}).getContent().size() == activations.size()

        // navigate using links
        assert restTemplate.exchange(
                menuPostResponse.getBody().getLink("activations").getHref() ,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<org.springframework.hateoas.Resources<Activation>>() {}
        ).getBody().getContent().size() == activations.size()


        // 4) CATEGORIES

        assert restTemplate.exchange(menuPostResponse.getBody().getLink("categories").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<org.springframework.hateoas.Resources<Category>>() {} )


        // 5) PLATES

        // search plates by menu unique name
        assert traverson.follow("plates","search","byMenuNaturalId").withTemplateParameters([
                owner : ownerPostResponse.getBody().getContent().getIdentifier(),
                name: menuPostResponse.getBody().getContent().getName()
        ]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Plate>>>() {}).getContent().size() == platePostResponseCollection.size()


        // navigate using links
        assert restTemplate.exchange(
                menuPostResponse.getBody().getLink("offers").getHref() ,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<org.springframework.hateoas.Resources<Plate>>() {}
        ).getBody().getContent().size() == platePostResponseCollection.size()

    }
}
