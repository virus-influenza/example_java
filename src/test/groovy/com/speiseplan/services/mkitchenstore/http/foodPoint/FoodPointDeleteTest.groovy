package com.speiseplan.services.mkitchenstore.http.foodPoint

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Category
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Location
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.config.security.NoSecurityOAuth2ServerConfiguration
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureLocation
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.domain.PageRequest
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.data.rest.webmvc.support.DefaultedPageable
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = [ Application, NoSecurityOAuth2ServerConfiguration ]
)
@WebAppConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "hsql", "populate", "no-security"])
class FoodPointDeleteTest {

    @Autowired
    private FoodPointRepository foodPointRepository
    @Autowired
    private MenuRepository menuRepository

    @Autowired
    private OwnerRepository ownerRepository

    @Autowired
    private RestTemplate httpTemplate

    @Value('${local.server.port}')
    int port

    private ResponseEntity<Resources<?>> discover
    private HttpHeaders headers
    private HttpHeaders uriList

    ResponseEntity<Resource<FoodPoint>> toDelete

    Resource<FoodPoint> foodPointSearchResponse

    @Before
    void setUp() {

        Traverson traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

        foodPointSearchResponse =  traverson.follow("foodpoints", "search","byName").withTemplateParameters([
                name: "searchable_point_B,_vegan_arab_lebanon" // check foodpoints.json, food point with no menus
        ]).toObject(new ParameterizedTypeReference<Resource<FoodPoint>>() {})

    }

    /**
     * delete a resource by natural id search
     */
    @Test
    void delete_resource_using_self_should_delete_entity() {


        def deleted = httpTemplate.exchange(
                new URI( foodPointSearchResponse.getLink("self").getHref() ),
                HttpMethod.DELETE,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resource<?>>() {}
        )

        assert deleted
        assert !foodPointRepository.findByName(foodPointSearchResponse.getContent().getName())
    }
}
