package com.speiseplan.services.mkitchenstore.http.foodPoint

import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateCategories
import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateOwners
import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.Category;
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.domain.types.Categories;
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureLocation
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.domain.Page
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = [
                Application
        ]
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "hsql-local", "populate-prod", "no-security"])
class FoodPointPostTest {
	
    @Autowired
    private FoodPointRepository foodPointRepository
    @Autowired
    private OwnerRepository ownerRepository

    @Autowired
    private RestTemplate httpTemplate // by name

    @Value('${local.server.port}')
    int port

    HttpHeaders halJson
    HttpHeaders textUriList
	Resource<Owner> ownerResoure

	Traverson traverson

	/* previous version
	ResponseEntity<Resource<Owner>> ownerResourceRE
	ResponseEntity<Resources<?>> discover
	*/
	
    @Before
    void setUp() {

        // headers
        halJson = new HttpHeaders()
        halJson.setContentType(MediaType.APPLICATION_JSON)
        halJson.setAccept([MediaTypes.HAL_JSON].asImmutable())

        textUriList = new HttpHeaders()
        textUriList.setContentType(RestMediaTypes.TEXT_URI_LIST)
        textUriList.setAccept([MediaTypes.HAL_JSON].asImmutable())

		// traverson hateoas client
		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)

		ownerResoure = traverson.follow("owners","search","byId").withTemplateParameters([
			id : ownerRepository.findAll().get( new Random().nextInt(ownerRepository.findAll().size()) ).pk
		]).toObject(new ParameterizedTypeReference<Resource<Owner>>() {})

		assert ownerResoure && ownerResoure.getContent()
	}

    @After
    void tearDown() {
        /*
		foodPointRepository.deleteAll()

        assert !foodPointRepository.findAll()
        */
    }

    @Test
    void post_simple_resource_should_create_entity() {

        Integer random = new Random().nextInt();

		Map<String,Object> foodPointMap = [
			name       : "FoodPoint_Random-$random" as String,
			displayName: "FoodPoint_Random-$random" as String,
			owner	   : ownerResoure.getLink("self").getHref(), //ownerResource.getBody().getLink("self").getHref(),
			location   : FixtureLocation.getOne()
		]

		def item = httpTemplate.exchange(
				traverson.follow("foodpoints").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(foodPointMap, halJson),
				new ParameterizedTypeReference<Resource<FoodPoint>>() {}
				)
        assert item
        assert item.getStatusCode() == HttpStatus.CREATED
        assert item.getBody()
        assert item.getBody().getContent()


        assert item
        assert foodPointRepository.findByPk(item.getBody().getContent().pk)

        // clean the house
        foodPointRepository.delete(item.getBody().getContent().pk)
        assert ! foodPointRepository.findByPk(item.getBody().getContent().pk)
    }


   @Test
   void post_resource_full_should_create_entity() {

        Integer random = new Random().nextInt();
		
		ResponseEntity<PagedResources<Resource<Category>>> categoriesResources = httpTemplate.exchange(
				traverson.follow("categories").asLink().getHref(),
				HttpMethod.GET,
				new HttpEntity(halJson),
				new ParameterizedTypeReference<PagedResources<Resource<Category>>>() {}
				)
		assert categoriesResources
		assert categoriesResources.statusCode == HttpStatus.OK

		Map<String,Object> foodPointMap = [
			name       : "FoodPoint_Random-$random" as String,
			displayName: "FoodPoint_Random-$random" as String,
			owner	   : ownerResoure.getLink("self").getHref(),
			location   : FixtureLocation.getOne(),
			categories : categoriesResources.getBody().getContent().collect{ it.getLink("self").getHref() }//,
//			offerCollections : [menuPostResponse.getBody().getLink("self").getHref()]
		]

		def foodPointResponse = httpTemplate.exchange(
				traverson.follow("foodpoints").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(foodPointMap, halJson),
				new ParameterizedTypeReference<Resource<FoodPoint>>() {}
				)
		assert foodPointResponse
		assert foodPointResponse.getStatusCode() == HttpStatus.CREATED
		assert foodPointResponse.getBody()
		assert foodPointResponse.getBody().getContent()
		
		def all = foodPointRepository.findAll()
		assert all
		assert all.every(){ it.pk }
		// assert all.every(){ it.categories } Is ok on EAGERT Test
		// There is not error but for now foodPoints must be put in offerCollections and not in reverse
		// assert all.every(){ !it.offerCollections } //Is Null on EAGER Test
		
		
		// EAGER foodpoint->owner  @OneToOne(fetch = FetchType.EAGER)
		assert httpTemplate.exchange(foodPointResponse.getBody().getLink("owner").getHref() , HttpMethod.GET, null, new ParameterizedTypeReference<Resource<Owner>>() {} ).getBody().getContent().getIdentifier() == ownerResoure.getContent().getIdentifier()

    }

}
