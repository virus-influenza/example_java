package com.speiseplan.services.mkitchenstore.http.foodPoint

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateOwners
import com.speiseplan.services.mkitchenstore.config.security.NoSecurityOAuth2ServerConfiguration
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.repository.OwnerRepository
import com.speiseplan.services.mkitchenstore.util.FixtureLocation
import com.speiseplan.services.mkitchenstore.util.FixtureOwner
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = [
                Application,
                PopulateOwners
        ]
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "hsql", "populate-owners", "no-security"])
class FoodPointSearchTest {

    @Autowired
    private FoodPointRepository foodPointRepository

    @Autowired OwnerRepository ownerRepository

    @Autowired
    private RestTemplate httpTemplate // by name

    @Value('${local.server.port}')
    int port

    private FoodPoint foodPoint
    private HttpHeaders headers
    private Owner owner
    private ResponseEntity<Resources<?>> discover

    @Before
    void setUp() {
        Integer random = new Random().nextInt();

        // request HATEOAS headers
        headers = new HttpHeaders()
        headers.setContentType(MediaType.APPLICATION_JSON)
        headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

        // known host and port
        discover = httpTemplate.exchange(
                new URI("http://localhost:${port}"),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        // known resource foodpoints
        discover = httpTemplate.exchange(
                new URI(discover.getBody().getLink("foodpoints").expand([:]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        // known operation search
        discover = httpTemplate.exchange(
                new URI(discover.getBody().getLink("search").getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        owner = ownerRepository.findAll().get(new Random().nextInt( ownerRepository.findAll().size() ))

        // tested entity
        foodPoint = foodPointRepository.save(new FoodPoint([
                name       : "Pizza_Der_Gregische-$random",
                displayName: "Pizza Der Gregische-$random",
                owner      : owner,
                location   : FixtureLocation.getOne()
        ]))

        assert foodPoint
    }

    @After
    void tearDown() {
        foodPointRepository.delete(foodPoint.pk)

        assert !foodPointRepository.findOne(foodPoint.pk)
    }

    @Test
    void findByNaturalId_should_find_resource() {
        // GET call using the search resource for food points, operation id, params name and zip code
        ResponseEntity<Resource<FoodPoint>> searched = httpTemplate.exchange(
                new URI(
                        discover.getBody().getLink("byNaturalId").expand([
                                name : foodPoint.name,
                                owner: foodPoint.owner.identifier,
                                projection: "withOwner"
                        ]).getHref()
                ),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resource<FoodPoint>>() {}
        )

        assert searched
        assert searched.getStatusCode() == HttpStatus.OK

        assert searched.getBody().getContent() == foodPoint
    }

    @Test
    void findByOwner_should_find_resources() {
        // GET call using the search resource for food points, operation id, params name and zip code
        ResponseEntity<PagedResources<FoodPoint>> searched = httpTemplate.exchange(
                new URI(
                        discover.getBody().getLink("byOwner").expand([
                                owner: foodPoint.owner.identifier,
                                projection: "withOwner"
                        ]).getHref()
                ),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<PagedResources<FoodPoint>>() {}
        )

        assert searched
        assert searched.getStatusCode() == HttpStatus.OK

        assert searched.getBody().getContent().contains(foodPoint)
    }

}
