package com.speiseplan.services.mkitchenstore.http.foodPoint

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.Resources
import org.springframework.hateoas.client.Traverson
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

/**
 *
 * @author virus.influenza
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
        classes = [
                Application
        ]
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "hsql-local", "populate", "no-security"])
class FoodPointGetTest {

    @Autowired
    private FoodPointRepository foodPointRepository

    @Autowired
    private RestTemplate httpTemplate //by name

    @Value('${local.server.port}')
    int port

    private Collection<FoodPoint> foodPoints
    private ResponseEntity<Resources<?>> discover
    private HttpHeaders headers
    private Traverson traverson


    @Before
    void setUp() {
        // have HAL fun :)
        // known host and port
        discover = httpTemplate.exchange(
                new URI("http://localhost:${port}"),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        headers = new HttpHeaders()
        headers.setContentType(MediaTypes.HAL_JSON)
        headers.setAccept([MediaTypes.HAL_JSON].asImmutable())

        foodPoints = foodPointRepository.findAll()

        assert foodPoints

        // traverson hateoas client
        traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)
    }

    @After
    void tearDown(){
    }


    @Test
    void get_collection_should_get_entities(){

        // known resource repository foodpoints
        // @see FoodPointRepository.@RepositoryResouce.path
        ResponseEntity<PagedResources<Resource<FoodPoint>>> collection = httpTemplate.exchange(
                new URI(discover.getBody().getLink("foodpoints").expand([:]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<FoodPoint>>>() {}
        )

        assert collection

        // response ok
        assert collection.getStatusCode() == HttpStatus.OK
        // headers
        assert collection.getHeaders()
        //body
        assert collection.getBody()
        // paged metadata, default spring pagination
        assert collection.getBody().getMetadata()
        assert collection.getBody().getMetadata().totalElements == foodPoints.size()


        // _self && _search links
        assert collection.getBody().getLinks()
        assert collection.getBody().getLink("self")
        assert collection.getBody().getLink("search")


        // resources
        assert collection.getBody().getContent()
        assert collection.getBody().getContent().size() == foodPoints.size()
        assert collection.getBody().getContent()*.getContent() == foodPoints
    }


    @Test
    void get_collection_with_size_and_page_should_get_entities(){

        // decorate test
        int size = 1
        int page = 0
        String projection = "withOwner"

        ResponseEntity<PagedResources<Resource<FoodPoint>>> collection = httpTemplate.exchange(
                new URI(discover.getBody().getLink("foodpoints").expand([
                        size:size,
                        page:page,
                        projection: projection
                ]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<FoodPoint>>>() {}
        )

        assert collection

        // response ok
        assert collection.getStatusCode() == HttpStatus.OK
        // headers
        assert collection.getHeaders()
        //body
        assert collection.getBody()
        // paginated information
        assert collection.getBody().getMetadata()
        assert collection.getBody().getMetadata().getNumber()        == page
        assert collection.getBody().getMetadata().getTotalPages()    == foodPoints.size()
        assert collection.getBody().getMetadata().getTotalElements() == foodPoints.size()
        assert collection.getBody().getMetadata().getSize()          == size

        // _self _search _next links
        assert collection.getBody().getLinks()
        assert collection.getBody().getLink("self")
        assert collection.getBody().getLink("search")
        assert collection.getBody().getLink("next") // only true if more than one food point in populated db

        // resources
        assert collection.getBody().getContent()
        assert collection.getBody().getContent().size() == size
        assert collection.getBody().getContent()[page].getContent() == foodPoints[page] // size 1 page 0  = first element :)
    }

    @Test
    void get_collection_sorted_should_get_entities(){

        /*
        sort=firstname,lastname
        sort=firstname,lastname,asc
        sort=firstname,lastname,desc
        sort=firstname,asc&sort=lastname,desc
         */

        String projection = "withOwner"

        ResponseEntity<PagedResources<Resource<FoodPoint>>> collection = httpTemplate.exchange(
                new URI(discover.getBody().getLink("foodpoints").expand([
                        sort:"name,desc",
                        projection: projection
                ]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<PagedResources<Resource<FoodPoint>>>() {}
        )

        assert collection

        // response ok
        assert collection.getStatusCode() == HttpStatus.OK
        // headers
        assert collection.getHeaders()
        //body
        assert collection.getBody()
        // paginable information, default spring pagination
        assert collection.getBody().getMetadata()
        assert collection.getBody().getMetadata().getTotalElements() == foodPoints.size()

        // _self && _search links
        assert collection.getBody().getLinks()
        assert collection.getBody().getLink("self")
        assert collection.getBody().getLink("search")

        // resources
        assert collection.getBody().getContent()
        assert collection.getBody().getContent().size() == foodPoints.size()
        assert collection.getBody().getContent().first().getContent() == foodPoints.sort{ it.name }.reverse().first() // desc

    }

    @Test
    void get_resource_should_get_entity(){

        String projection = "withOwner"


        discover = httpTemplate.exchange(
                new URI(discover.getBody().getLink("foodpoints").expand([:]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        discover = httpTemplate.exchange(
                new URI(discover.getBody().getLink("search").expand([:]).getHref()),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resources<?>>() {}
        )

        ResponseEntity<Resource<FoodPoint>> found = httpTemplate.exchange(
                new URI( discover.getBody().getLink("byNaturalId").expand([
                        name   :foodPoints[0].name,
                        owner  :foodPoints[0].owner.identifier,
                        projection: projection
                ]).getHref() ),
                HttpMethod.GET,
                new HttpEntity<Object>(headers),
                new ParameterizedTypeReference<Resource<FoodPoint>>() {}
        )

        assert found

        assert found.getStatusCode() == HttpStatus.OK
        assert found.getBody()
        assert found.getBody().getContent()
        assert found.getBody().getContent() == foodPoints[0]
    }


    @Test
    void with_traverson_get_collection(){


        assert Boolean.TRUE


    }

}
