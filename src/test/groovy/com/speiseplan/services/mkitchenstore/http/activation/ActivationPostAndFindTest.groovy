package com.speiseplan.services.mkitchenstore.http.activation

import com.speiseplan.services.mkitchenstore.config.Application
import com.speiseplan.services.mkitchenstore.domain.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.core.ParameterizedTypeReference
import org.springframework.data.rest.webmvc.RestMediaTypes
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.PagedResources
import org.springframework.hateoas.Resource
import org.springframework.hateoas.client.Traverson
import org.springframework.http.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.web.client.RestTemplate

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * 
 * @author nak
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(
		classes = [
				Application
		]
)
@WebAppConfiguration
@TransactionConfiguration
@IntegrationTest("server.port:0")
@ActiveProfiles(["test", "hsql-local", "populate-prod", "no-security"])
class ActivationPostAndFindTest {

	@Autowired
	@Qualifier("httpTemplate")
	RestTemplate restTemplate

	@Value('${local.server.port}')
	int port

	private Traverson traverson

	private HttpHeaders halJson
	private HttpHeaders textUriList
	
	private ResponseEntity<Resource<Owner>> ownerPostResponse
	
	@Before
	void setUp() {

		// traverson hateoas client
		traverson = new Traverson(new URI("http://localhost:${port}/"), MediaTypes.HAL_JSON)
		traverson.setRestOperations(restTemplate)

		// headers
		// application/hal+json
		halJson = new HttpHeaders()
		halJson.setContentType(MediaTypes.HAL_JSON)
		halJson.setAccept([MediaTypes.HAL_JSON].asImmutable())

		// text/uri-list
		textUriList = new HttpHeaders()
		textUriList.setContentType(RestMediaTypes.TEXT_URI_LIST)
		textUriList.setAccept([MediaTypes.HAL_JSON].asImmutable())

		// 1) create an owner ( owner )
		Map<String,Object> owner = [
				identifier: "myActivationPostTestOwner${new Random().nextInt()}" as String
		]

		ownerPostResponse = restTemplate.exchange(
				traverson.follow("owners").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(owner, halJson),
				new ParameterizedTypeReference<Resource<Owner>>() {}
		)

		assert ownerPostResponse
		assert ownerPostResponse.statusCode == HttpStatus.CREATED

	}
	
	@Test
	void post_simple_resource_should_create_entity(){

		// 1) The OfferCollection is mandatory
		Map<String,Object> menu = [
			name       : "notNullName${new Random().nextInt()}" as String,
			displayName: "notNullDisplayName",
			owner      : ownerPostResponse.getBody().getLink("self").getHref(),
		]
		def menuPostResponse = restTemplate.exchange(
				traverson.follow("menus").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(menu, halJson),
				new ParameterizedTypeReference<Resource<Menu>>() {}
				)
		assert menuPostResponse
		assert menuPostResponse.statusCode == HttpStatus.CREATED

		Map<String,Object> activation = [
					start      : LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
					end        : LocalDate.now().plusDays(5).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
					owner      : ownerPostResponse.getBody().getLink("self").getHref(),
					offerCollection: menuPostResponse.getBody().getLink("self").getHref()
			]


		def activationPostResult = restTemplate.exchange(
					traverson.follow("activations").asLink().getHref(),
					HttpMethod.POST,
					new HttpEntity(activation, halJson),
					new ParameterizedTypeReference<Resource<Activation>>() {}
			)

		assert activationPostResult
		assert activationPostResult.statusCode == HttpStatus.CREATED
		
		//**********************************************************************
		//************* Query Test *********************************************
		//**********************************************************************


		/**
		 *find by owner
		 */
		def activationSearchResponse = traverson.follow("activations","search","byOwner").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		/**
		 * find by owner and startDate is after query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byOwnerAndActive").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byOwnerAndActive").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by owner and startDate is before query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byOwnerAndInactive").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		activationSearchResponse = traverson.follow("activations","search","byOwnerAndInactive").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
	
	
	}

	@Test
	void post_full_resource_should_create_entity(){

		// 1) The OfferCollection is mandatory
		Map<String,Object> menu = [
			name       : "notNullName${new Random().nextInt()}" as String,
			displayName: "notNullDisplayName",
			owner      : ownerPostResponse.getBody().getLink("self").getHref(),
		]
		def menuPostResponse = restTemplate.exchange(
				traverson.follow("menus").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(menu, halJson),
				new ParameterizedTypeReference<Resource<Menu>>() {}
				)
		assert menuPostResponse
		assert menuPostResponse.statusCode == HttpStatus.CREATED
		
		// 2) An Origin
		Map<String,Object> foodPoint = [
			name       : "notNullName${new Random().nextInt()}" as String,
			displayName: "notNullDisplayName",
			owner      : ownerPostResponse.getBody().getLink("self").getHref(),
			location   : new Location(latitude:19.432608f , longitude:-99.133208f )
		]
		def foodPointPostResponse = restTemplate.exchange(
				traverson.follow("foodpoints").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(foodPoint, halJson),
				new ParameterizedTypeReference<Resource<FoodPoint>>() {}
				)
		assert foodPointPostResponse
		assert foodPointPostResponse.statusCode == HttpStatus.CREATED

		// 4) At last we can Create the Activation
		Map<String,Object> activation = [
			start      : LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
			end        : LocalDate.now().plusDays(5).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
			owner      : ownerPostResponse.getBody().getLink("self").getHref(),
			origin     : foodPointPostResponse.getBody().getLink("self").getHref(),
			offerCollection: menuPostResponse.getBody().getLink("self").getHref()
		]
		def activationPostResult = restTemplate.exchange(
				traverson.follow("activations").asLink().getHref(),
				HttpMethod.POST,
				new HttpEntity(activation, halJson),
				new ParameterizedTypeReference<Resource<Activation>>() {}
				)
		assert activationPostResult
		assert activationPostResult.statusCode == HttpStatus.CREATED

		//**********************************************************************
		//************* Query Test *********************************************
		//**********************************************************************
		
		/**
		 * find by owner
		 */
		def activationSearchResponse = traverson.follow("activations","search","byOwner").withTemplateParameters([
			owner: ownerPostResponse.getBody().getContent().identifier
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1

		/**
		 * find by menu pk
		 */
		activationSearchResponse = traverson.follow("activations","search","byMenu").withTemplateParameters([
			menu: menuPostResponse.getBody().getContent().pk
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byMenu").withTemplateParameters([
			menu: 99999L
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by menu name
		 */
		activationSearchResponse = traverson.follow("activations","search","byMenuName").withTemplateParameters([
			name: menuPostResponse.getBody().getContent().name
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byMenuName").withTemplateParameters([
			name: 'whatever'
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by Menu pk and Activation start date after query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byMenuAndActive").withTemplateParameters([
			menu: menuPostResponse.getBody().getContent().pk,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byMenuAndActive").withTemplateParameters([
			menu: menuPostResponse.getBody().getContent().pk,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by Menu pk and Activation start date before query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byMenuAndInactive").withTemplateParameters([
			menu: menuPostResponse.getBody().getContent().pk,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		activationSearchResponse = traverson.follow("activations","search","byMenuAndInactive").withTemplateParameters([
			menu: menuPostResponse.getBody().getContent().pk,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		/**
		 * find by foodPoint pk
		 */
		activationSearchResponse = traverson.follow("activations","search","byFoodPoint").withTemplateParameters([
			foodpoint: foodPointPostResponse.getBody().getContent().pk
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byFoodPoint").withTemplateParameters([
			foodpoint: 999999L
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by foodPoint pk and Activation start date after query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byFoodPointAndActive").withTemplateParameters([
			foodpoint: foodPointPostResponse.getBody().getContent().pk,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
		activationSearchResponse = traverson.follow("activations","search","byFoodPointAndActive").withTemplateParameters([
			foodpoint: foodPointPostResponse.getBody().getContent().pk,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		/**
		 * find by foodPoint pk and Activation start date before query date
		 */
		activationSearchResponse = traverson.follow("activations","search","byFoodPointAndInactive").withTemplateParameters([
			foodpoint: foodPointPostResponse.getBody().getContent().pk,
			start: LocalDate.now().minusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 0
		
		activationSearchResponse = traverson.follow("activations","search","byFoodPointAndInactive").withTemplateParameters([
			foodpoint: foodPointPostResponse.getBody().getContent().pk,
			start: LocalDate.now().plusDays(3).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
		]).toObject(new ParameterizedTypeReference<PagedResources<Resource<Activation>>>() {})
		assert activationSearchResponse.getContent().size() == 1
		
	}
}
