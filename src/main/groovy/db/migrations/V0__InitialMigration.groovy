package db.migrations

import org.flywaydb.core.api.migration.spring.SpringJdbcMigration
import org.springframework.jdbc.core.JdbcTemplate

class V0__InitialMigration implements SpringJdbcMigration{

    public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
        jdbcTemplate.execute("""
          SELECT 1 FROM DUAL
        """)
    }

}
