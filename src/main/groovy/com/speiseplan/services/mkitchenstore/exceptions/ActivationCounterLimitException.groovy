package com.speiseplan.services.mkitchenstore.exceptions

class ActivationCounterLimitException extends RuntimeException{

	ActivationCounterLimitException(String message){
		super(message)
	}

}
