package com.speiseplan.services.mkitchenstore.exceptions


class BadUserException extends RuntimeException{

	BadUserException(String message){
		super(message)
	}
}
