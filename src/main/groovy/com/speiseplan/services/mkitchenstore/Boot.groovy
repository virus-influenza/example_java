package com.speiseplan.services.mkitchenstore

import com.speiseplan.services.mkitchenstore.config.Application
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication

/**
 *
 * @author virus.influenza
 */
class Boot {

    private static final Logger LOG = LoggerFactory.getLogger(Boot.class);

    public static void main(String[] args) {

        LOG.info("************************************************")
        LOG.info("WELCOME TO MFOODPOINT HUMAN                     ")
        LOG.info("command line arguments:                          ")
        LOG.info("{}                                              ", args.each {return it})
        LOG.info("available profiles:")
        LOG.info("     environments: dev, prod, test")
        LOG.info("     configurations: mysql, hsql, eureka, rabbitmq, events")
        LOG.info("user may select each profile to activate the correlated technology")
        LOG.info("names are quite obvious so just check application.yml for detailed configuration")
        LOG.info("")
        LOG.info("*********************************************************************************")


        def context = SpringApplication.run(Application.class, args)




        println context
    }
}
