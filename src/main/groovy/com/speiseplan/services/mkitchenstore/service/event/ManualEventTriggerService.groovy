package com.speiseplan.services.mkitchenstore.service.event

interface ManualEventTriggerService<I,O> {

	O triggerSingleResourceReindexByPk(I request)
}
