package com.speiseplan.services.mkitchenstore.service.event

import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import com.speiseplan.services.mkitchenstore.event.manual.ReindexResourceEventHandler
import com.speiseplan.services.mkitchenstore.model.response.OfferCollectionReindexResponse
import com.speiseplan.services.mkitchenstore.repository.MenuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
class OfferCollectionManualEventTriggerService implements ManualEventTriggerService<Long, OfferCollectionReindexResponse>{

	@Autowired
	MenuRepository menuRepository
	
	@Autowired
	ReindexResourceEventHandler<Menu> menuReindexResourceEventHandler
		
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public OfferCollectionReindexResponse triggerSingleResourceReindexByPk(Long pk) {
		
		if(!pk){
			throw new IllegalArgumentException("Id not valid")
		}
		
		Menu menu = menuRepository.findByPk(pk)

		if(!menu){
			throw new IllegalArgumentException("resource not found")
		}

		menuReindexResourceEventHandler.handleEvent(menu, ResourceEvents.RESPAWN)

		return new OfferCollectionReindexResponse(pk: menu.pk);
	}

}
