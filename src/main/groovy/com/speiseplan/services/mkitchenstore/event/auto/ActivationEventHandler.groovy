package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Activation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * 
 * @author nak
 *
 */
@Component
@RepositoryEventHandler
class ActivationEventHandler extends AbstractResourceEventHandler<Activation> {

	@Autowired
	RabbitResourceEventHandler<Activation> rabbitResourceEventHandler

	@HandleBeforeCreate
	void handleBeforeCreate(Activation resource) {
		super.handleBeforeCreate(resource)
	}
	
	@HandleAfterCreate
	void handleAfterCreate(Activation resource) {
		super.handleAfterCreate(resource)
	}

	@HandleBeforeSave
	void handleBeforeSave(Activation resource) {
		super.handleBeforeSave(resource)
	}

   
	@HandleAfterSave
	void handleAfterSave(Activation resource) {
		super.handleAfterSave(resource)
	}

	@HandleBeforeDelete
	void handleBeforeDelete(Activation resource) {
		super.handleBeforeDelete(resource)
	}

   
	@HandleAfterDelete
	void handleAfterDelete(Activation resource) {
		super.handleAfterDelete(resource)
	}
}
