package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Plate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * 
 * @author nak
 *
 */
@Component
@RepositoryEventHandler
class OfferEventHandler extends AbstractResourceEventHandler<Plate> {

	@Autowired
	RabbitResourceEventHandler<Plate> rabbitResourceEventHandler

	@HandleBeforeCreate
	void handleBeforeCreate(Plate resource) {
		super.handleBeforeCreate(resource)
	}
	
	@HandleAfterCreate
	void handleAfterCreate(Plate resource) {
		super.handleAfterCreate(resource)
	}

	@HandleBeforeSave
	void handleBeforeSave(Plate resource) {
		super.handleBeforeSave(resource)
	}

   
	@HandleAfterSave
	void handleAfterSave(Plate resource) {
		super.handleAfterSave(resource)
	}

	@HandleBeforeDelete
	void handleBeforeDelete(Plate resource) {
		super.handleBeforeDelete(resource)
	}

	@HandleAfterDelete
	void handleAfterDelete(Plate resource) {
		super.handleAfterDelete(resource)
	}
}
