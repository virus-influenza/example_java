package com.speiseplan.services.mkitchenstore.event

/**
 * Created by pakito on 7/06/17.
 */
enum ResourceEvents {
    beforeCreate,
    afterCreate,
    beforeSave,
    afterSave,
    beforeDelete,
    afterDelete,
    afterLinkSave,
    afterLinkDelete,
    RESPAWN
}