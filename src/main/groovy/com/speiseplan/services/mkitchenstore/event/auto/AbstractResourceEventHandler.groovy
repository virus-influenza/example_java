package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.event.ResourceEvents

/**
 * Created by pakito on 7/06/17.
 */
abstract class AbstractResourceEventHandler<T> {

    abstract RabbitResourceEventHandler<T> getRabbitResourceEventHandler()

    void handleBeforeCreate(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.beforeCreate)
    }

    void handleAfterCreate(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.afterCreate)
    }

    void handleBeforeSave(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.beforeSave)
    }

    void handleAfterSave(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.afterSave)
    }

    void handleBeforeDelete(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.beforeDelete)
    }

    void handleAfterDelete(T resource) {
        getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.afterDelete)
    }
	
	void handleFullReindex(T resource) {
		getRabbitResourceEventHandler().handleEvent(resource,ResourceEvents.RESPAWN)
	}
}
