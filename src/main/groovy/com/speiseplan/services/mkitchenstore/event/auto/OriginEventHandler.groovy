package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * 
 * @author nak
 *
 */
@Component
@RepositoryEventHandler
class OriginEventHandler extends AbstractResourceEventHandler<FoodPoint> {

	@Autowired
	RabbitResourceEventHandler<FoodPoint> rabbitResourceEventHandler

	@HandleBeforeCreate
	void handleBeforeCreate(FoodPoint resource) {
		super.handleBeforeCreate(resource)
	}
	
	@HandleAfterCreate
	void handleAfterCreate(FoodPoint resource) {
		super.handleAfterCreate(resource)
	}

	@HandleBeforeSave
	void handleBeforeSave(FoodPoint resource) {
		super.handleBeforeSave(resource)
	}

   
	@HandleAfterSave
	void handleAfterSave(FoodPoint resource) {
		super.handleAfterSave(resource)
	}

	@HandleBeforeDelete
	void handleBeforeDelete(FoodPoint resource) {
		super.handleBeforeDelete(resource)
	}

   
	@HandleAfterDelete
	void handleAfterDelete(FoodPoint resource) {
		super.handleAfterDelete(resource)
	}
}
