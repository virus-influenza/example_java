package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Category
import com.speiseplan.services.mkitchenstore.domain.Menu
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * 
 * @author nak
 *
 */
@Component
@RepositoryEventHandler
class CategoryEventHandler extends AbstractResourceEventHandler<Category> {

	@Autowired
	RabbitResourceEventHandler<Menu> rabbitResourceEventHandler

	@HandleBeforeCreate
	void handleBeforeCreate(Category resource) {
		super.handleBeforeCreate(resource)
	}
	
	@HandleAfterCreate
	void handleAfterCreate(Category resource) {
		super.handleAfterCreate(resource)
	}

	@HandleBeforeSave
	void handleBeforeSave(Category resource) {
		super.handleBeforeSave(resource)
	}

	@HandleAfterSave
	void handleAfterSave(Category resource) {
		super.handleAfterSave(resource)
	}

	@HandleBeforeDelete
	void handleBeforeDelete(Category resource) {
		super.handleBeforeDelete(resource)
	}

	@HandleAfterDelete
	void handleAfterDelete(Category resource) {
		super.handleAfterDelete(resource)
	}
}
