package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Owner
import com.speiseplan.services.mkitchenstore.domain.Plate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * 
 * @author nak
 *
 */
@Component
@RepositoryEventHandler
class OwnerEventHandler extends AbstractResourceEventHandler<Owner> {

	@Autowired
	RabbitResourceEventHandler<Plate> rabbitResourceEventHandler

	@HandleBeforeCreate
	void handleBeforeCreate(Owner resource) {
		super.handleBeforeCreate(resource)
	}
	
	@HandleAfterCreate
	void handleAfterCreate(Owner resource) {
		super.handleAfterCreate(resource)
	}

	@HandleBeforeSave
	void handleBeforeSave(Owner resource) {
		super.handleBeforeSave(resource)
	}

   
	@HandleAfterSave
	void handleAfterSave(Owner resource) {
		super.handleAfterSave(resource)
	}

	@HandleBeforeDelete
	void handleBeforeDelete(Owner resource) {
		super.handleBeforeDelete(resource)
	}

   
	@HandleAfterDelete
	void handleAfterDelete(Owner resource) {
		super.handleAfterDelete(resource)
	}
}
