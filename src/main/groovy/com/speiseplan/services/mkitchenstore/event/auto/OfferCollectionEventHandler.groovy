package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Menu
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.*
import org.springframework.stereotype.Component

/**
 * Created by pakito on 24/05/17.
 */
@Component
@RepositoryEventHandler
class OfferCollectionEventHandler extends AbstractResourceEventHandler<Menu> {

    @Autowired
    RabbitResourceEventHandler<Menu> rabbitResourceEventHandler

    @Override
    @HandleBeforeCreate
    void handleBeforeCreate(Menu resource) {
        super.handleBeforeCreate(resource)
    }

    @HandleAfterCreate
    void handleAfterCreate(Menu resource) {
        super.handleAfterCreate(resource)
    }

    @HandleBeforeSave
    void handleBeforeSave(Menu resource) {
        super.handleBeforeSave(resource)
    }

   
    @HandleAfterSave
    void handleAfterSave(Menu resource) {
        super.handleAfterSave(resource)
    }

   
    @HandleBeforeDelete
    void handleBeforeDelete(Menu resource) {
        super.handleBeforeDelete(resource)
    }

   
    @HandleAfterDelete
    void handleAfterDelete(Menu resource) {
        super.handleAfterDelete(resource)
    }
}
