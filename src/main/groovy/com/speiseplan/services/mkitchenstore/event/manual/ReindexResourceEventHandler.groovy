package com.speiseplan.services.mkitchenstore.event.manual

import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ReindexResourceEventHandler<T> {

	@Autowired
	RabbitTemplate rabbitJsonTemplate

	@Value('${mkitchenstore.amqp.definition.general.exchange.name}')
	String destinationTopicExchange

	@Value('${spring.application.name}')
	String applicationName

	@Value('${mkitchenstore.amqp.definition.general.technology}')
	String technology

    public void handleEvent(T resource, ResourceEvents event){

        rabbitJsonTemplate.convertAndSend(
                destinationTopicExchange,
                "${applicationName}.${technology}.${event}.${resource.class.simpleName}.${event}",
                [
                        resource: resource,
                ]
        )
    }

}
