package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/**
 * Created by pakito on 7/06/17.
 */
@Component
class RabbitResourceEventHandler<T> {

    @Autowired
    RabbitTemplate rabbitJsonTemplate

    @Value('${mkitchenstore.amqp.definition.general.exchange.name}')
    String destinationTopicExchange

    @Value('${spring.application.name}')
    String applicationName

    @Value('${mkitchenstore.amqp.definition.general.technology}')
    String technology

    void handleEvent(T resource, ResourceEvents event){
        rabbitJsonTemplate.convertAndSend(
                destinationTopicExchange,
                "${applicationName}.${technology}.${event}.${resource.class.simpleName}",
                [
                        resource: resource,
                ]
        )
    }
}
