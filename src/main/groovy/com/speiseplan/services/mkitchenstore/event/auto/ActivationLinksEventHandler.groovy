package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Activation
import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.HandleAfterLinkDelete
import org.springframework.data.rest.core.annotation.HandleAfterLinkSave
import org.springframework.data.rest.core.annotation.RepositoryEventHandler
import org.springframework.stereotype.Component

/**
 *
 * @author virus.influenza
 */
@Component
@RepositoryEventHandler
class ActivationLinksEventHandler {

    @Autowired
    RabbitLinkEventHandler<Activation> rabbitLinkEventHandler

    @HandleAfterLinkSave
    void handleAfterLinkSave(Activation activation, Collection childs) {
        rabbitLinkEventHandler.handleEvent(activation, childs, ResourceEvents.afterLinkSave)
    }


    @HandleAfterLinkDelete
    void handleAfterLinkDelete(Activation activation, Collection childs) {
        rabbitLinkEventHandler.handleEvent(activation, childs, ResourceEvents.afterLinkDelete)
    }
}
