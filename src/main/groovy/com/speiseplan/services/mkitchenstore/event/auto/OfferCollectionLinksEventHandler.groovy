package com.speiseplan.services.mkitchenstore.event.auto

import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.event.ResourceEvents
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.core.annotation.HandleAfterLinkDelete
import org.springframework.data.rest.core.annotation.HandleAfterLinkSave
import org.springframework.data.rest.core.annotation.RepositoryEventHandler
import org.springframework.stereotype.Component

/**
 *
 * @author virus.influenza
 */
@Component
@RepositoryEventHandler
class OfferCollectionLinksEventHandler {

    @Autowired
    RabbitLinkEventHandler<Menu> rabbitLinkEventHandler

    @HandleAfterLinkSave
    void handleAfterLinkSave(Menu menu, Collection childs) {
        rabbitLinkEventHandler.handleEvent(menu,childs, ResourceEvents.afterLinkSave)
    }

    @HandleAfterLinkDelete
    void handleAfterLinkDelete(Menu menu, Collection childs) {
        rabbitLinkEventHandler.handleEvent(menu,childs, ResourceEvents.afterLinkDelete)
    }
}
