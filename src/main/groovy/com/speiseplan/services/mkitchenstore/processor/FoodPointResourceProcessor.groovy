package com.speiseplan.services.mkitchenstore.processor

import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import org.springframework.hateoas.Resource
import org.springframework.hateoas.ResourceProcessor
import org.springframework.stereotype.Component

@Component
class FoodPointResourceProcessor implements ResourceProcessor<Resource<FoodPoint>> {


    @Override
    Resource<FoodPoint> process(Resource<FoodPoint> resource) {


        resource.getLinks().remove( resource.getLink("foodPoint") ) // it is just ugly
        return resource
    }
}
