package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.Plate
import com.speiseplan.services.mkitchenstore.domain.types.Plates
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 *
 * @author virus.influenza
 */
@RepositoryRestResource(
        path = "plates",
        collectionResourceRel = "plates"
)
interface PlateRepository extends
        JpaRepository<Plate, Long>, SearchableByPk<Plate> {

    @RestResource(path = "byNaturalId", rel = "byNaturalId")
    Plate findByOwnerIdentifierAndNameAndOfferType(
            @Param("owner") String owner,
            @Param("name") String name,
            @Param("type") Plates type
    )

    @RestResource(path = "byOwner", rel = "byOwner")
    Page<Plate> findByOwnerIdentifier(
            @Param("owner") String owner,
            Pageable pageable
    )

    @RestResource(path = "byMenuPk", rel = "byMenuPk")
    Page<Plate> findByOfferCollectionsPk(
            @Param("pk") Long menu,
            Pageable pageable
    )

    @RestResource(path = "byMenuNaturalId", rel = "byMenuNaturalId")
    Page<Plate> findByOwnerIdentifierAndOfferCollectionsName(
            @Param("owner") String owner,
            @Param("name") String name,
            Pageable pageable
    )
}