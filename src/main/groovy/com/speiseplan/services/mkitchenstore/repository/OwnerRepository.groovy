package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.Owner
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(
        path = "owners",
        collectionResourceRel = "owners"
)
interface OwnerRepository extends
        JpaRepository<Owner, Long>, SearchableByPk<Owner>{
}
