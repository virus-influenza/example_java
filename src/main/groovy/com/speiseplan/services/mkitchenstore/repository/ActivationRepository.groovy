package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.Activation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource
import org.springframework.format.annotation.DateTimeFormat

import java.time.LocalDate

/**
 *
 * @author virus.influenza
 */
@RepositoryRestResource(
        path = "activations",
        collectionResourceRel = "activations"
)
interface ActivationRepository extends
        JpaRepository<Activation, Long>, SearchableByPk<Activation> {

    @RestResource(path="byNaturalId", rel = "byNaturalId")
    Activation findByPk(
            @Param("id") Long id
    )

    /**
     * Find activations by owner, for every food point and every menu
     * @param owner owner
     * @param pageable pagination
     * @return
     */
    @RestResource(path = "byOwner", rel = "byOwner")
    Page<Activation> findAllByOwnerIdentifier(
            @Param("owner") String owner,
            Pageable pageable
    )


    /**
     * Find all active activations by owner, for every food point and every menu
     * An Active Activation (tm) is defined by an activation happening now or in the future
     * @param owner owner
     * @param start from which date to start searching for activations forward
     * @param pageable pagination
     * @return
     */
    @RestResource(
            path = "byOwnerAndActive",
            rel = "byOwnerAndActive"
    )
    Page<Activation> findAllByOwnerIdentifierAndStartGreaterThanEqualOrderByStartAsc(
            @Param("owner") String owner,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
            Pageable pageable
    )

    /**
     * Find all inactive activations by owner, for every food point and every menu
     * An Inactive Activation (tm) is defined by an activation already past
     * @param owner owner
     * @param start from which date to start searching for activations backwards
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byOwnerAndInactive", rel = "byOwnerAndInactive")
	Page<Activation> findAllByOwnerIdentifierAndStartBeforeOrderByStartDesc(
			@Param("owner") String owner,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)

    /**
     * Find activations of a Menu defined by its primary key
     * @param menu primary key, menu identifier
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byMenu", rel = "byMenu")
	Page<Activation> findAllByOfferCollectionPkOrderByStart(
			@Param("menu") Long menu,
			Pageable pageable
	)

	/**
	 * Find activations of a Menu defined by its unique immutable name
	 * @param menu unique immutable name
	 * @param pageable pagination
	 * @return
	 */
	@RestResource(path = "byMenuName", rel = "byMenuName")
	Page<Activation> findByOfferCollectionNameOrderByStartAsc(
			@Param("name") String name,
			Pageable pageable
	)


    /**
     * Find active activations of a Menu defined by its primary key
     * @param menu primary key, menu identifier
     * @param start from which date to start searching for activations forward
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byMenuAndActive", rel = "byMenuAndActive")
	Page<Activation> findAllByOfferCollectionPkAndStartGreaterThanEqualOrderByStartAsc(
			@Param("menu") Long menu,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)

    /**
     * Find inactive past activations of a Menu defined by its primary key
     * @param menu primary key, menu identifier
     * @param start from which date to start searching for activations backwards
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byMenuAndInactive", rel = "byMenuAndInactive")
	Page<Activation> findAllByOfferCollectionPkAndStartBeforeOrderByStartDesc(
			@Param("menu") Long menu,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)

    /**
     * Find inactive past activations of a FoodPoint defined by its primary key
     * @param food point primary key, food point identifier
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byFoodPoint", rel = "byFoodPoint")
	Page<Activation> findByOriginPk(
			@Param("foodpoint") Long foodPoint,
			Pageable pageable
	)
	/*@RestResource(path = "byFoodPoint", rel = "byFoodPoint")
	Page<Activation> findByOfferCollectionOriginsPk(
			@Param("foodpoint") Long foodPoint,
			Pageable pageable
	)*/

    /**
     * Find active future activations of a FoodPoint defined by its primary key
     * @param food point primary key, food point identifier
     * @param start from which date to start searching for activations forward
     * @param pageable pagination
     * @return
     */
	/*@RestResource(path = "byFoodPointAndActive", rel = "byFoodPointAndActive")
	Page<Activation> findByOfferCollectionOriginsPkAndStartGreaterThanEqualOrderByStartAsc(
			@Param("foodpoint") Long foodpoint,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)*/
	/**
	 * Find active future activations of a FoodPoint defined by its primary key
	 * @param food point primary key, food point identifier
	 * @param start from which date to start searching for activations forward
	 * @param pageable pagination
	 * @return
	 */
	@RestResource(path = "byFoodPointAndActive", rel = "byFoodPointAndActive")
	Page<Activation> findByOriginPkAndStartGreaterThanEqualOrderByStartAsc(
			@Param("foodpoint") Long foodpoint,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)

    /**
     * Find inactive past activations of a FoodPoint defined by its primary key
     * @param food point primary key, food point identifier
     * @param start from which date to start searching for activations backwards
     * @param pageable pagination
     * @return
     */
	@RestResource(path = "byFoodPointAndInactive", rel = "byFoodPointAndInactive")
	Page<Activation> findByOriginPkAndStartBefore(
			@Param("foodpoint") Long foodpoint,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)
	/*@RestResource(path = "byFoodPointAndInactive", rel = "byFoodPointAndInactive")
	Page<Activation> findByOfferCollectionOriginsPkAndStartBefore(
			@Param("foodpoint") Long foodpoint,
			@DateTimeFormat(pattern = "dd/MM/yyyy") @Param("start") LocalDate start,
			Pageable pageable
	)*/
}