package com.speiseplan.services.mkitchenstore.repository

import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RestResource


interface SearchableByPk<T> {

    @RestResource(path="byId", rel = "byId", exported = true)
    T findByPk(
            @Param("id") Long id
    )
}
