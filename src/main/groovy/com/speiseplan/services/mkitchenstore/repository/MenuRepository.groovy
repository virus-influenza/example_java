package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.Menu
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 *
 * @author virus.influenza
 */
@RepositoryRestResource(
        path = "menus",
        collectionResourceRel = "menus"
)
interface MenuRepository extends
        JpaRepository<Menu, Long>, SearchableByPk<Menu> {


    @RestResource(path="byNaturalId", rel = "byNaturalId")
    Menu findByOwnerIdentifierAndName(
            @Param("owner") String owner,
            @Param("name") String name
    )

    @RestResource(path = "byOwner", rel = "byOwner")
    Page<Menu> findByOwnerIdentifier(
            @Param("owner") String owner,
            Pageable pageable
    )

    @RestResource(path="byFoodPointId", rel = "byFoodPointId")
    @Query("Select Distinct M From Menu M Inner Join M.activations A Inner Join A.origin F Where F.pk = :foodPoint ")
    Page<Menu> findByOriginPk(
            @Param("foodPoint") Long foodPoint,
            Pageable pageable
    )

    @RestResource(path="byFoodPointNaturalId", rel = "byFoodPointNaturalId")
    @Query("Select Distinct M From Menu M Inner Join M.activations A Inner Join A.origin F Inner Join F.owner O Where O.identifier = :owner And F.name = :name ")
    Page<Menu> findByOriginNaturalKey(
            @Param("owner") String owner,
            @Param("name") String name,
            Pageable pageable
    )

    /*@RestResource(path="byFoodPointId", rel = "byFoodPointId")
    Page<Menu> findByOriginsPk(
            @Param("id") Long foodPoint,
            Pageable pageable
    )

    @RestResource(path="byFoodPointNaturalId", rel = "byFoodPointNaturalId")
    Page<Menu> findByOriginsOwnerIdentifierAndOriginsName(
            @Param("owner") String owner,
            @Param("name") String name,
            Pageable pageable
    )*/

}
