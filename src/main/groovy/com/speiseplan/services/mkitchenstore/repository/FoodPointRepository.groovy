package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.projection.FoodPointWithOwnerProjection
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 *
 * @author virus.influenza
 */
@RepositoryRestResource(
        path = "foodpoints",
        collectionResourceRel = "foodpoints",
        excerptProjection = FoodPointWithOwnerProjection
)
interface FoodPointRepository extends
        JpaRepository<FoodPoint, Long>, SearchableByPk<FoodPoint> {

    @RestResource(path="byNaturalId", rel = "byNaturalId")
    FoodPoint findByNameAndOwnerIdentifier(
            @Param("name") String name,
            @Param("owner") String owner
    )

    @RestResource(path="byName", rel = "byName")
    FoodPoint findByName(
            @Param("name") String name
    )


    @RestResource(path="byOwner", rel = "byOwner")
    Page<FoodPoint> findByOwnerIdentifier(
            @Param("owner") String owner,
            Pageable pageable
    )

   /* @RestResource(path="byMenuName", rel = "byMenuName")
    Page<FoodPoint> findByOfferCollectionsName(
            @Param("name") String name,
            Pageable pageable
    )*/

}