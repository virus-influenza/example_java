package com.speiseplan.services.mkitchenstore.repository

import com.speiseplan.services.mkitchenstore.domain.Category
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

/**
 *
 * @author virus.influenza
 */
@RepositoryRestResource(
        path = "categories",
        collectionResourceRel = "categories"
)
interface FoodCategoryRepository extends
        JpaRepository<Category, Long>, SearchableByPk<Category> {

    @RestResource(path = "byNaturalId", rel="byNaturalId")
    Category findByNameAndOfferCategory(
            @Param("name") String name,
            @Param("type") Categories offerCategory
    )

    @RestResource(path = "byType", rel="byType")
    Page<Category> findByOfferCategory(
            @Param("type") Categories offerCategory,
            Pageable pageable
    )
}
