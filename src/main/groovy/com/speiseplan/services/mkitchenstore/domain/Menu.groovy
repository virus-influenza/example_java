package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Identified
import com.speiseplan.domain.mkitchencontext.resource.definition.OfferCollection
import com.speiseplan.domain.mkitchencontext.resource.definition.UniqueName
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.time.LocalDateTime

/**
 *
 * @author virus.influenza
 */
@ToString(
        includeNames = true,
        includePackage = false,
        includes = ["owner", "name"]
)
@EqualsAndHashCode(
        includes = ["owner", "name"]
)
@Table(
        name = "menu",
        uniqueConstraints = [
                @UniqueConstraint(
                        name = "menu_unique_natural_key",
                        columnNames = ["name"]
                )
        ]
)
@EntityListeners(AuditingEntityListener.class)
@Entity
class Menu implements
        Serializable,
        Identified,
        OfferCollection,
        UniqueName {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pk")
    @JsonProperty
    Long pk

	@NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "menu_owner", 
		joinColumns = @JoinColumn(name = "menu_id"), 
		inverseJoinColumns = @JoinColumn(name = "owner_id"))
    Owner owner

    @NotNull
    @Size(min = 1)
    @Column(name = "name", nullable = false, updatable = false)
    @JsonProperty(required = true)
    String name

    @NotNull
    @Size(min = 1)
    @Column(name = "display_name", nullable = false)
    @JsonProperty(required = true)
    String displayName

	@OneToMany(mappedBy = "offerCollection")
    Set<Activation> activations

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "menu_categories")
    Set<Category> categories

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "menu_plates")
    Collection<Plate> offers

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "menu_food_points")
//    Set<FoodPoint> origins

    @CreatedDate
    @JsonIgnore
    LocalDateTime created

    @LastModifiedDate
    @JsonIgnore
    LocalDateTime modified

    @Override
    Map<String, Object> identifiedBy() {
        return [owner: owner.identifier, name: name]
    }

}
