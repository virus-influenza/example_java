package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Identified
import com.speiseplan.domain.mkitchencontext.resource.definition.Offer
import com.speiseplan.domain.mkitchencontext.resource.definition.UniqueName
import com.speiseplan.services.mkitchenstore.domain.types.Plates
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.NaturalId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.time.LocalDateTime

/**
 *
 * @author virus.influenza
 */
@ToString(
        includePackage = false,
        includeNames = true,
        includes = ["owner","name","offerType"]
)
@EqualsAndHashCode(
        includes = ["owner","name","offerType"]
)
@Table(
        name="plate",
        uniqueConstraints = [
                @UniqueConstraint(
                        name = "menu_unique_natural_key",
                        columnNames = ["name", "offer_type"]   // owner cannot be constraint  defined like that
                )
        ]
)
@EntityListeners(AuditingEntityListener.class)
@Entity
class Plate implements
        Serializable,
        Identified,
        Offer,
        UniqueName {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pk")
    @JsonProperty
    Long pk

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "plate_owner", 
		joinColumns = @JoinColumn(name = "plate_id"), 
		inverseJoinColumns = @JoinColumn(name = "owner_id"))
    Owner owner

    @NotNull
    @Size(min=1)
    @NaturalId
    @Column(name="name", updatable = false, nullable = false)
    @JsonProperty(required = true)
    String name

    @NotNull
    @Size(min=1)
    @Column(name="display_name", nullable = false)
    @JsonProperty(required = true)
    String displayName

    @NotNull
    @Column(name="offer_type", nullable = false)
    @Enumerated(EnumType.STRING)
    Plates offerType

    @Column(name="description")
    String description

	@JsonBackReference
    @ManyToMany(mappedBy = "offers")
    Set<Menu> offerCollections

    @CreatedDate
    @JsonIgnore
    LocalDateTime created

    @LastModifiedDate
    @JsonIgnore
    LocalDateTime modified

    @Override
    Map<String, Object> identifiedBy() {
        return [owner:owner, name:name]
    }
}
