package com.speiseplan.services.mkitchenstore.domain.projection

import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Menu
import org.springframework.data.rest.core.config.Projection

/**
 * Created by pakito on 30/05/16.
 */
@Projection(name = "withFoodPoints" , types = Menu.class)
interface MenuWithFoodPointsProjection {

    Long getPk()
    String getOwner()
    String getName()

    String getDisplayName()

    Set<FoodPoint> getOrigins()
}
