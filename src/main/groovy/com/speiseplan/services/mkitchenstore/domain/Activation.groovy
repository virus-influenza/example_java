package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import java.time.LocalDate
import java.time.LocalDateTime

@ToString(
        includes = ["start", "end", "owner","offerCollection","origins"],
        includePackage = false
)
@EqualsAndHashCode(
        includes = ["start", "end", "owner","offerCollection"]
)
@EntityListeners(AuditingEntityListener.class)
@Table(name = "activation")
@Entity
class Activation implements Serializable, com.speiseplan.domain.mkitchencontext.resource.definition.Activation{


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pk")
    @JsonProperty
    Long pk

	@NotNull
	@OneToOne(fetch = FetchType.EAGER) // ¿, cascade = CascadeType.ALL ? !!
	@JoinTable(name="activation_owner",joinColumns=@JoinColumn(name="activation_id"),inverseJoinColumns= @JoinColumn(name="owner_id"))
    Owner owner
	
	@JsonBackReference
	@NotNull
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="menu_id")
	Menu offerCollection

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="origin_id")
	FoodPoint origin

    @NotNull
    @Column(name="start_date", updatable = false, nullable = false)
    @JsonProperty(required=true)
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate start

    @NotNull
    @Column(name="end_date", updatable = false, nullable = false)
    @JsonProperty(required=true)
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate end

    @CreatedDate
    @JsonIgnore
    LocalDateTime created

    @LastModifiedDate
    @JsonIgnore
    LocalDateTime modified

    @java.beans.Transient
    Map<String, Object> identifiedBy() {
        return [id:pk]
    }

}
