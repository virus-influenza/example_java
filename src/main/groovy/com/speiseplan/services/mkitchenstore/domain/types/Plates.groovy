package com.speiseplan.services.mkitchenstore.domain.types

import com.speiseplan.domain.mkitchencontext.resource.definition.types.OfferType
import groovy.transform.CompileStatic

/**
 *
 * @author virus.influenza
 */
@CompileStatic
enum Plates implements OfferType{
    STARTER,
    MAIN,
    SECOND,
    DESSERT,
    EXTRA;

    @Override
    OfferType getOfferType() {
        return this
    }
}