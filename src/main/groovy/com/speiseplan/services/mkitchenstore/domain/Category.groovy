package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Identified
import com.speiseplan.domain.mkitchencontext.resource.definition.UniqueName
import com.speiseplan.services.mkitchenstore.domain.types.Categories
import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.NaturalId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.time.LocalDateTime

/**
 *
 * @author virus.influenza
 */
@CompileStatic
@ToString(
        includes = ["name", "offerCategory"],
        includePackage = false,
        includeNames = true
)
@EqualsAndHashCode(
        includes = ["name","offerCategory"]
)
@EntityListeners(AuditingEntityListener.class)
@Table(
        name="category",
        uniqueConstraints=[
                @UniqueConstraint(name="category_unique_natural_key", columnNames = ["name", "type"] )
        ]
)
@Entity
class Category implements
        Serializable,
        Identified,
        com.speiseplan.domain.mkitchencontext.resource.definition.Category,
        UniqueName{

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="pk")
    @JsonProperty
    Long pk

    @NotNull
    @Size(min = 1)
    @NaturalId
    @Column(name = "name", nullable = false, updatable = false)
    @JsonProperty(required = true)
    String name

    @NotNull
    @Size(min = 1)
    @Column(name = "display_name", nullable = false)
    @JsonProperty(required = true)
    String displayName

    @NotNull
    @NaturalId
    @Column(name="type", updatable = false, nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonProperty(required = true)
    Categories offerCategory

    @Column(name="description")
    String description

    @CreatedDate
    @JsonIgnore
    LocalDateTime created

    @LastModifiedDate
    @JsonIgnore
    LocalDateTime modified

    @Override
    Map<String, Object> identifiedBy() {
        return [name:name, type:offerCategory]
    }
}
