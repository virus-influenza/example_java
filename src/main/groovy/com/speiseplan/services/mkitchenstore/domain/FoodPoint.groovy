package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Identified
import com.speiseplan.domain.mkitchencontext.resource.definition.Origin
import com.speiseplan.domain.mkitchencontext.resource.definition.UniqueName
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.time.LocalDateTime

@ToString(
        includeNames = true,
        includePackage = false,
        includes = ["owner", "name", "location"]
)
@EqualsAndHashCode(
        includes = ["owner", "name"]
)
@Table(
        name = "food_point",
        uniqueConstraints = [
                @UniqueConstraint(
                        name = "food_point_unique_natural_key",
                        columnNames = ["name"]
                )]
)
@EntityListeners(AuditingEntityListener.class)
@Entity
class FoodPoint implements
        Serializable,
        Identified,
        Origin,
        UniqueName {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pk")
    @JsonProperty
    Long pk

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinTable(
            name = "food_point_owner",
            joinColumns = @JoinColumn(name = "food_point_id"),
            inverseJoinColumns = @JoinColumn(name = "owner_id")
    )
    Owner owner

    @NotNull
    @Size(min = 1)
    @Column(name = "name", nullable = false, updatable = false)
    @JsonProperty(required = true)
    String name

    @NotNull
    @Size(min = 1)
    @Column(name = "display_name", nullable = false)
    @JsonProperty(required = true)
    String displayName

    @NotNull
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "food_point_location",
		joinColumns = @JoinColumn(name = "food_point_id"),
		inverseJoinColumns = @JoinColumn(name = "location_id"))
    Location location

    @ManyToMany
    @JoinTable(name = "food_point_categories")
    Set<Category> categories

//    @ManyToMany(mappedBy = "origins")
//    Set<Menu> offerCollections

    @JsonBackReference
	@OneToMany(mappedBy = "origin")
    Set<Activation> activations
	
    @CreatedDate
    @JsonIgnore
    LocalDateTime created

    @LastModifiedDate
    @JsonIgnore
    LocalDateTime modified

    @Override
    Map<String, Object> identifiedBy() {
        return [owner: owner.identifier, name: name]
    }
}
