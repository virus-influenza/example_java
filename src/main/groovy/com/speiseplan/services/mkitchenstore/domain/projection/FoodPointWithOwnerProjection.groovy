package com.speiseplan.services.mkitchenstore.domain.projection

import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Location
import com.speiseplan.services.mkitchenstore.domain.Owner
import org.springframework.data.rest.core.config.Projection

/**
*/
@Projection(name = "withOwner" , types = FoodPoint.class)
interface FoodPointWithOwnerProjection {

    Long getPk()
    String getName()
    String getDisplayName()
    Owner getOwner()
    Location getLocation()

}
