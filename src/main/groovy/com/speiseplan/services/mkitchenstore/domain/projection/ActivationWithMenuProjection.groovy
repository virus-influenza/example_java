package com.speiseplan.services.mkitchenstore.domain.projection

import com.speiseplan.services.mkitchenstore.domain.Activation
import com.speiseplan.services.mkitchenstore.domain.FoodPoint
import com.speiseplan.services.mkitchenstore.domain.Menu
import com.speiseplan.services.mkitchenstore.domain.Owner
import org.springframework.data.rest.core.config.Projection

import java.time.LocalDate

/**
 * Created by pakito on 30/05/16.
 */
@Projection(name = "expand" , types = Activation.class)
interface ActivationWithMenuProjection {

    LocalDate getStart()
    LocalDate getEnd()

	Menu getOfferCollection()
	Set<FoodPoint> getOrigins()
	
	/*It coul be just the string in this way:: @Value("#{target.getOwner().getIdentifier()}")*/
	Owner getOwner()
}
