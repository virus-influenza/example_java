package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Owner as OwnerContext
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.NaturalId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.time.LocalDateTime

@ToString(
	includes = ["identifier"],
	includePackage = false
)
@EqualsAndHashCode(
	includes = ["identifier"]
)
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
@Entity
class Owner implements OwnerContext{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk")
	@JsonProperty
	Long pk
	
	@NotNull
	@Size(min = 1)
	@NaturalId
	@Column(name = "identifier", nullable = false, updatable = false)
	@JsonProperty(required = true)
	String identifier

	@CreatedDate
	@JsonIgnore
	LocalDateTime created

	@LastModifiedDate
	@JsonIgnore
	LocalDateTime modified

}
