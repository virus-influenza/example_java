package com.speiseplan.services.mkitchenstore.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.speiseplan.domain.mkitchencontext.resource.definition.Identified
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.*
import java.time.LocalDateTime

@ToString(
	includeNames = true,
	includePackage = false,
	includes = ["city", "address", "zipCode", "latitude", "longitude"]
)
@EqualsAndHashCode
@Table(name = "location")
@EntityListeners(AuditingEntityListener.class)
@Entity
class Location implements com.speiseplan.domain.mkitchencontext.resource.definition.Location, Identified{


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pk")
	@JsonProperty
	Long pk

	@Column(name="description")
	String description

	@Column(name="city")
	String city
	
	@Column(name="address")
	String address

	@Column(name="zipCode")
	String zipCode
	
	@Column(name = "latitude", nullable = true)
	@JsonProperty
	Double latitude

	@Column(name = "longitude", nullable = true)
	@JsonProperty
	Double longitude

	@CreatedDate
	@JsonIgnore
	LocalDateTime created

	@LastModifiedDate
	@JsonIgnore
	LocalDateTime modified

	@Override
	Map<String, Object> identifiedBy() {
		return [pk:this.pk]
	}
}
