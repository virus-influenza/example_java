package com.speiseplan.services.mkitchenstore.domain.types

import com.speiseplan.domain.mkitchencontext.resource.definition.types.OfferCategory
import groovy.transform.CompileStatic

/**
 *
 * @author virus.influenza
 */
@CompileStatic
enum Categories implements OfferCategory{

    ROOT,
    MAIN,
    SUB;

    @Override
    OfferCategory getOfferCategory() {
        return this
    }
}