package com.speiseplan.services.mkitchenstore.util

import com.speiseplan.services.mkitchenstore.exceptions.BadUserException
import groovy.util.logging.Log4j
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import java.security.Principal

/**
 * Check if user log and filter owner are the same entity 
 * @author nak
 *
 */
@Component
@Log4j
@Profile("anti-hacking")
class UserAntiHackingFilter implements Filter{

	protected static final String REQUEST_USER_PARAMETER = "owner"

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
		String owner = request.getParameter(REQUEST_USER_PARAMETER)

		if (! HttpMethod.GET.toString().toLowerCase().equals(request.getMethod()?.toLowerCase()) || !owner){
			chain.doFilter(req, res);
			return;
		}

		Principal principal = request.getUserPrincipal()

		/** User Roles Info For Future Filters
		Object o = principal.getProperties().get('authorities')
		List<String> roles = o as List<String>
		roles.each {
			log.info it
		}
		*/
		
		if(!SecurityUtils.INSTANCE.isOwnerParamEqualsToUser(owner, principal?.name)){ // && roleNotIn(...)
			log.error 'Owner param and logged User must be the same'
			throw new BadUserException("Owner param and logged User must be the same")
		}
	
        chain.doFilter(req, res)
    }

    public void init(FilterConfig filterConfig) {}

    public void destroy() {}

}
