package com.speiseplan.services.mkitchenstore.util

import com.speiseplan.async.anachronistic.events.domain.EventOperation
import org.springframework.hateoas.core.EvoInflectorRelProvider


enum WordUtils {
    INSTANCE,

    public String pluralize(String input) {
        return "${input}s"
    }

    public String pluralize(Class clazz) {
        return "${ new EvoInflectorRelProvider().getCollectionResourceRelFor(clazz).toLowerCase() }"
    }

    public String eventToRoutingKey(EventOperation.EVENTS event) {

        if ( event.name().count("_") > 1 ){
            return event.name().toLowerCase().replaceAll("_",".").replaceFirst("\\.","_")
        }

        return event.name().toLowerCase().replaceAll("_",".")
    }

}