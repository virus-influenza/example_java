package com.speiseplan.services.mkitchenstore.config.security

/**
 *
 * @author virus.influenza
 */
class AuthorizationServerDetails {

    String tokenEndpointUrl

    String clientId
    String clientSecret

}
