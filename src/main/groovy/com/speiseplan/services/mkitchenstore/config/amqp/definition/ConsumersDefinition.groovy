package com.speiseplan.services.mkitchenstore.config.amqp.definition
/**
 *
 * @author virus.influenza
 */
class ConsumersDefinition {

    Integer concurentConsumers
    Integer maxConcurrentConsumers
}
