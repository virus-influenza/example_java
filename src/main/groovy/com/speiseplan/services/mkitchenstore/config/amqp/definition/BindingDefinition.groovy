package com.speiseplan.services.mkitchenstore.config.amqp.definition

import org.springframework.amqp.core.Binding.DestinationType

/**
 *
 * @author virus.influenza
 */
class BindingDefinition {

    String destination
    String exchange
    DestinationType destinationType
    String routingKey
}
