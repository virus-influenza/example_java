package com.speiseplan.services.mkitchenstore.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.introspect.AnnotatedMember
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.speiseplan.services.mkitchenstore.domain.*
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration
import org.springframework.data.web.config.EnableSpringDataWebSupport
import org.springframework.hateoas.hal.Jackson2HalModule
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.sync.diffsync.web.JsonPatchHttpMessageConverter
import org.springframework.web.servlet.HandlerInterceptor

import javax.annotation.Resource

/**
 *
 * @author virus.influenza
 */
@Import([RepositoryRestMvcConfiguration])
@Configuration
@EnableSpringDataWebSupport
class Rest extends RepositoryRestConfigurerAdapter{

    @Resource
    HandlerInterceptor localeChangeInterceptor

    /**
     * @Deprecated
     * http://stackoverflow.com/questions/19251846/custom-default-headers-for-rest-api-only-using-spring-data-rest
     * i18n
     * @return
     */

    /**
     * return body of updated or created entity when POST/PUT/PATCH methods are executed
     * @param config
     */
    @Override
    void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        super.configureRepositoryRestConfiguration(config)

        config.setReturnBodyOnCreate(Boolean.TRUE)
        config.setReturnBodyOnUpdate(Boolean.TRUE)

        config.exposeIdsFor(FoodPoint,Menu,Plate,Category,Activation)
    }


    @Override
    void configureHttpMessageConverters(List<HttpMessageConverter<?>> messageConverters){
        super.configureHttpMessageConverters(messageConverters)

        messageConverters.add(new JsonPatchHttpMessageConverter())
    }

    /**
     * hacking fun with groovy and spring :)
     * @param objectMapper
     */
    @Override
    void configureJacksonObjectMapper(ObjectMapper objectMapper) {
        super.configureJacksonObjectMapper(objectMapper)

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.setAnnotationIntrospector(new IgnoreMetaclassIntrospector())

        objectMapper.registerModule(new Jackson2HalModule()) // HAL
        objectMapper.registerModule(new JavaTimeModule()) // Java dates
    }

    private static class IgnoreMetaclassIntrospector extends JacksonAnnotationIntrospector {
        @Override
        public boolean hasIgnoreMarker(final AnnotatedMember m) {
            return m.getMember().getName() == "setMetaClass" || super.hasIgnoreMarker(m);
        }
    }
}
