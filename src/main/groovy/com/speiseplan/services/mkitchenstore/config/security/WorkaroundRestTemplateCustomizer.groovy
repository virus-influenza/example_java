package com.speiseplan.services.mkitchenstore.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.netflix.ribbon.RibbonClientHttpRequestFactory
import org.springframework.cloud.security.oauth2.resource.UserInfoRestTemplateCustomizer
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.stereotype.Component

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
class WorkaroundRestTemplateCustomizer implements UserInfoRestTemplateCustomizer {

	@Autowired RibbonClientHttpRequestFactory ribbonClientHttpRequestFactory
	
    @Override
    public void customize(OAuth2RestTemplate template) {
		template.setRequestFactory(ribbonClientHttpRequestFactory);
    }

}
