package com.speiseplan.services.mkitchenstore.config.pupulate

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean

@Configuration
@Profile(["populate-categories"])
class PopulateCategories {


    @Autowired ApplicationContext applicationContext

    @Bean
    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {


        Resource categories  = new ClassPathResource("data/production_categories.json")

        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.registerModule(new JavaTimeModule()) // for joda date times

        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean()
        factory.setMapper(mapper)
        factory.setResources( [ categories ] as Resource[] )

        return factory
    }
}
