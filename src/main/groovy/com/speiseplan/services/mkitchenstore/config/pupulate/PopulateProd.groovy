package com.speiseplan.services.mkitchenstore.config.pupulate

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean

@Configuration
@Profile(["populate-prod"])
class PopulateProd {

	@Autowired ApplicationContext applicationContext

	@Bean
	public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {


		Resource owners  = new ClassPathResource("data/owners_prod.json")
		//Resource categories  = new ClassPathResource("data/production_categories.json")
		
		Resource locations   = new ClassPathResource("data/locations_prod.json")
		Resource foodpoints  = new ClassPathResource("data/foodpoints_prod.json")
		Resource categories  = new ClassPathResource("data/categories_prod.json")
		Resource activations = new ClassPathResource("data/activations_prod.json")
		Resource menus       = new ClassPathResource("data/menus_prod.json")
		Resource plates      = new ClassPathResource("data/plates_prod.json")


		ObjectMapper mapper = new ObjectMapper()
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		mapper.registerModule(new JavaTimeModule())

		Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean()
		factory.setMapper(mapper)
		factory.setResources( [ owners, locations, categories, plates, foodpoints, menus, activations ] as Resource[] )

		return factory
	}

}
