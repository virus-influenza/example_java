package com.speiseplan.services.mkitchenstore.config.http

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.webmvc.convert.UriListHttpMessageConverter
import org.springframework.hateoas.MediaTypes
import org.springframework.hateoas.ResourceSupport
import org.springframework.hateoas.hal.Jackson2HalModule
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter
import org.springframework.http.MediaType
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.sync.diffsync.web.JsonPatchHttpMessageConverter
import org.springframework.web.client.RestTemplate

/**
 * @author virus.influenza
 */
@Configuration
class Template {


    @Bean(name = "httpTemplate")
    public RestTemplate httpTemplate() {

        List<HttpMessageConverter> converters = new ArrayList<>()

        // whatever
        // for traverson, it is using response type String with media type HAL+JSON, *halConverter will dramatically fail
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter()
        stringHttpMessageConverter.setSupportedMediaTypes([MediaType.ALL, MediaType.TEXT_PLAIN])
        converters.add(new StringHttpMessageConverter())

        // add HAL converter
        converters.add(halConverter())
        converters.add(typedHalConverter())
        // add JsonPatch spring sync converter for json+patch operations
        converters.add(new JsonPatchHttpMessageConverter())
        // add UriList spring converter for uri-list operations
        converters.add(new UriListHttpMessageConverter())

        RestTemplate result = new RestTemplate()

        result.setRequestFactory(new HttpComponentsClientHttpRequestFactory(HttpClientBuilder.create().build()))
        result.setMessageConverters(converters)

        return result
    }

    @Bean
    public MappingJackson2HttpMessageConverter halConverter() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

        mapper.registerModule(new Jackson2HalModule()) // HAL
        mapper.registerModule(new JavaTimeModule()) // Java dates
        //mapper.registerModule(new GeoJsonModule()) // GEO JSON
        //mapper.registerModule(new GeoModule()) // Spring GEO

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter()
        converter.setSupportedMediaTypes([MediaType.APPLICATION_JSON, MediaTypes.HAL_JSON].asImmutable())
        converter.setObjectMapper(mapper)


        TypeConstrainedMappingJackson2HttpMessageConverter

        return converter
    }


    @Bean
    public TypeConstrainedMappingJackson2HttpMessageConverter typedHalConverter() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

        mapper.registerModule(new Jackson2HalModule()) // HAL
        mapper.registerModule(new JavaTimeModule()) // HAL
        //mapper.registerModule(new GeoJsonModule()) // GEO JSON
        //mapper.registerModule(new GeoModule()) // Spring GEO

        MappingJackson2HttpMessageConverter converter = new TypeConstrainedMappingJackson2HttpMessageConverter(ResourceSupport)
        converter.setSupportedMediaTypes([MediaTypes.HAL_JSON].asImmutable())
        converter.setObjectMapper(mapper)


        TypeConstrainedMappingJackson2HttpMessageConverter

        return converter
    }

}
