package com.speiseplan.services.mkitchenstore.config.jdbc

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType

import javax.sql.DataSource

/**
 *
 * @author virus.influenza
 */
@Configuration
@Profile(["hsql","hsql-local"])
class HSQLDataSource {

    /**
     * The default scripts are:
     * {@code "schema.sql"} to create the database schema and
     * {@code "data.sql"} to populate the database with data.
     * @return
     */
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build()
    }

}
