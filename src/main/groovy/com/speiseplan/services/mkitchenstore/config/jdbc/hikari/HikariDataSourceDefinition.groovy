package com.speiseplan.services.mkitchenstore.config.jdbc.hikari

import groovy.transform.CompileStatic
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 *
 * @author virus.influenza
 */
// reloadable
@CompileStatic
@Component
@ConfigurationProperties(
        prefix = "mkitchenstore.jdbc.dataSource"
)
class HikariDataSourceDefinition {

    String url
    String user
    String password
    Boolean cachePrepStmts
    Integer prepStmtCacheSize
    Integer prepStmtCacheSqlLimit
    Boolean useServerPrepStmts
}
