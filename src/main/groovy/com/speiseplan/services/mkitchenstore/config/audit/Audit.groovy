package com.speiseplan.services.mkitchenstore.config.audit

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.repository.config.EnableJpaAuditing


@Configuration
@EnableJpaAuditing
@Profile("audit")
class Audit {
}
