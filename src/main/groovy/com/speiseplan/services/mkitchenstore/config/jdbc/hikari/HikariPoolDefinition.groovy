package com.speiseplan.services.mkitchenstore.config.jdbc.hikari

import groovy.transform.CompileStatic
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 *
 * @author virus.influenza
 */
// reloadable
@CompileStatic
@Component
@ConfigurationProperties(
        prefix = "mkitchenstore.jdbc",
        ignoreNestedProperties = true
)
class HikariPoolDefinition {

    String dataSourceClassName
    String poolName
    String connectionTestQuery
    Integer maximumPoolSize
    Integer idleTimeout

}
