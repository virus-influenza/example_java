package com.speiseplan.services.mkitchenstore.config.amqp.definition
/**
 *
 * @author virus.influenza
 */
class TopicExchangeDefinition {

    String identifier
    String name
    Boolean durable
    Boolean autoDelete
}
