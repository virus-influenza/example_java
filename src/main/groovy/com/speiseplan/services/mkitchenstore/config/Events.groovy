package com.speiseplan.services.mkitchenstore.config

import com.speiseplan.services.mkitchenstore.event.auto.AbstractResourceEventHandler
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

/**
 *
 * @author virus.influenza
 */
@Configuration
@ComponentScan(
        basePackageClasses = [AbstractResourceEventHandler]
)
@Profile(["events"])
class Events {
}