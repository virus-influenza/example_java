package com.speiseplan.services.mkitchenstore.config.amqp

import com.speiseplan.services.mkitchenstore.config.amqp.definition.BindingDefinition
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * AMQP components
 * @author virus.influenza
 */
@Configuration
@ConfigurationProperties(
        prefix="mkitchenstore.amqp.definition"
)
class Bindings {

    List<BindingDefinition> bindings

    @Bean
    public List<org.springframework.amqp.core.Binding> allBindings(){

        Collection<org.springframework.amqp.core.Binding> result = []

        bindings.each{ BindingDefinition binding ->
            result << new org.springframework.amqp.core.Binding(
                    binding.destination,
                    binding.destinationType,
                    binding.exchange,
                    binding.routingKey,
                    [:]
            )
        }

        return result
    }

}
