package com.speiseplan.services.mkitchenstore.config.amqp

import com.speiseplan.services.mkitchenstore.config.amqp.definition.TopicExchangeDefinition
import org.springframework.amqp.core.TopicExchange
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 *
 * @author virus.influenza
 */
@Configuration
@ConfigurationProperties(
        prefix="mkitchenstore.amqp.definition"
)
class Exchanges {

    List<TopicExchangeDefinition> exchanges

    @Bean
    public List<TopicExchange> allTopicExchanges(){

        Collection<TopicExchange> result = []
        exchanges.each{ TopicExchangeDefinition exchange ->
            result << new TopicExchange(
                    exchange.name,
                    exchange.durable,
                    exchange.autoDelete
            )
        }

        return result
    }


}
