package com.speiseplan.services.mkitchenstore.config.pupulate

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean

/**
 *
 * @author virus.influenza
 */
@Configuration
@Profile(["populate"])
class Populate {

    @Autowired ApplicationContext applicationContext

    @Bean
    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {

		Resource users       = new ClassPathResource("data/owners.json")
		Resource locations   = new ClassPathResource("data/locations.json")
        Resource foodpoints  = new ClassPathResource("data/foodpoints.json")
        Resource categories  = new ClassPathResource("data/categories.json")
        Resource activations = new ClassPathResource("data/activations.json")
        Resource menus       = new ClassPathResource("data/menus.json")
        Resource plates      = new ClassPathResource("data/plates.json")

        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.registerModule(new JavaTimeModule()) // for java 8 date times

        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean()
        factory.setMapper(mapper)
        factory.setResources( [users, locations, categories, plates, foodpoints, menus, activations ] as Resource[] )
        //factory.setResources( [ foodpoints ] as Resource[] )

        return factory
    }
}
