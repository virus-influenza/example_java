package com.speiseplan.services.mkitchenstore.config.amqp

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.annotation.EnableRabbit
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.hateoas.hal.Jackson2HalModule

import javax.annotation.Resource

/**
 *
 * @author virus.influenza
 */
@Configuration
@Import([
        Exchanges.class,
        Bindings.class,
        Queues.class
])
@EnableRabbit
@Profile(["rabbitmq","rabbitmq-local"])
class SimpleRabbitMQ {

    @Resource
    ConnectionFactory rabbitConnectionFactory

    @Resource
    List<TopicExchange> allTopicExchanges
    @Resource
    List<org.springframework.amqp.core.Binding> allBindings
    @Resource
    List<org.springframework.amqp.core.Queue> allQueues


    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(rabbitConnectionFactory)

        return rabbitTemplate
    }

    @Bean
    public RabbitTemplate rabbitJsonTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(rabbitConnectionFactory)
        Jackson2JsonMessageConverter converter =  new Jackson2JsonMessageConverter()

        ObjectMapper mapper = new ObjectMapper()
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        mapper.registerModule(new JavaTimeModule()) // Java time

        converter.setJsonObjectMapper(mapper)


        rabbitTemplate.setMessageConverter(converter)

        return rabbitTemplate
    }


    @Bean
    public RabbitAdmin rabbitAdmin() {
        RabbitAdmin admin = new RabbitAdmin(rabbitConnectionFactory)

        allTopicExchanges.each { TopicExchange exchange ->
            admin.declareExchange(exchange)
        }
        allQueues.each { org.springframework.amqp.core.Queue queue ->
            admin.declareQueue(queue)
        }
        allBindings.each { org.springframework.amqp.core.Binding binding ->
            admin.declareBinding(binding)
        }

        return admin
    }
}
