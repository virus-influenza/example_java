package com.speiseplan.services.mkitchenstore.config.security

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer

@Configuration
@EnableResourceServer
@Profile("no-security")
class NoSecurityOAuth2ServerConfiguration extends OAuth2ServerConfiguration {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off

		http
		.antMatcher("/**")
		.authorizeRequests()
		.anyRequest().permitAll()
	}
}
