package com.speiseplan.services.mkitchenstore.config

import com.speiseplan.services.mkitchenstore.controller.event.OfferCollectionManualEventController
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@ComponentScan(
	basePackageClasses = [
			OfferCollectionManualEventController
	]
)
@Configuration
@Profile(["web"])
class Web extends WebMvcConfigurerAdapter {
   
	@Override
    void addFormatters(FormatterRegistry registry) {
        super.addFormatters(registry)
    }
}
