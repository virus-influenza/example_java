package com.speiseplan.services.mkitchenstore.config.i18n

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import org.springframework.web.servlet.i18n.SessionLocaleResolver

/**
 * i18n context configuration
 * @author virus.influenza
 */
@Configuration
class Internationalization {

    /**
     * i18n
     */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver()
        slr.setDefaultLocale(Locale.US)
        return slr
    }

    /**
     * i18n
     * @return
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor()
        lci.setParamName("lang")
        return lci
    }
}
