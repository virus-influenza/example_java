package com.speiseplan.services.mkitchenstore.config

import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile

/**
 *
 * @author virus.influenza
 */
@EnableEurekaClient
@Configuration
@Profile("eureka")
class Eureka {
}
