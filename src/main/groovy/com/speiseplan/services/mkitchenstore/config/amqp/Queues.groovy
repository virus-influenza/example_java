package com.speiseplan.services.mkitchenstore.config.amqp

import com.speiseplan.services.mkitchenstore.config.amqp.definition.QueueDefinition
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * AMQP components
 * @author virus.influenza
 */
@Configuration
@ConfigurationProperties(
        prefix="mkitchenstore.amqp.definition"
)
class Queues {

    List<QueueDefinition> queues

    @Bean
    public List<org.springframework.amqp.core.Queue> allQueues(){

        Collection<org.springframework.amqp.core.Queue> result = []

        queues.each{ QueueDefinition binding ->
            result << new org.springframework.amqp.core.Queue(
                    binding.name,
                    binding.durable,
                    binding.exclusive?:Boolean.FALSE,
                    binding.autoDelete?:Boolean.FALSE,
                    [:]
            )
        }

        return result
    }

}
