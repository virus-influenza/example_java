package com.speiseplan.services.mkitchenstore.config.amqp.definition

/**
 *
 * @author virus.influenza
 */
class QueueDefinition {
    String name
    Boolean durable
    Boolean exclusive
    Boolean autoDelete
}
