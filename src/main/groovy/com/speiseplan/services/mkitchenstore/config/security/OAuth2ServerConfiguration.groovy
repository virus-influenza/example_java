package com.speiseplan.services.mkitchenstore.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationTrustResolver
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager
import org.springframework.security.oauth2.provider.token.AccessTokenConverter
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter
import org.springframework.security.oauth2.provider.token.RemoteTokenServices
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.util.FileCopyUtils

@Configuration
@EnableResourceServer
@Profile("security")
class OAuth2ServerConfiguration  extends ResourceServerConfigurerAdapter {

	@Value('${mkitchenstore.security.server.resource_id:mauthentication}')
	String resourceId;
	
	@Value('${mkitchenstore.security.server.required_role:ROLE_OWNER}')
	String defaultRole;

	@Value('${mkitchenstore.security.server.tokenEndpointUrl:http://localhost:8083/oauth/check_token}')
	String tokenEndpointUrl

    @Value('${mkitchenstore.security.server.clientId:"human"}')
    String clientId

    @Value('${mkitchenstore.security.server.clientSecret:"0123456789"}')
    String clientSecret
	
	@Autowired(required = false)
	List<AuthenticationTrustResolver> trustResolvers = new ArrayList<>();
	
	@Autowired(required = false)
	List<org.springframework.security.access.PermissionEvaluator> permissionEvaluators = new ArrayList<>();
	
	@Autowired
	ApplicationContext context
	
	@Bean
	public AccessTokenConverter accessTokenConverter() {
		
		return new DefaultAccessTokenConverter()
	}
	
	@Bean
	public AuthenticationManager authenticationManager() throws Exception {
		OAuth2AuthenticationManager authenticationManager = new OAuth2AuthenticationManager()
		authenticationManager.setTokenServices(remoteTokenServices())
		return authenticationManager
	}
	
	@Bean
	public RemoteTokenServices remoteTokenServices() {
		final RemoteTokenServices remoteTokenServices = new RemoteTokenServices()
		remoteTokenServices.setCheckTokenEndpointUrl(tokenEndpointUrl) //" oauth/check_token v1/user http://localhost:8083/v1/user mauthentication-test
		remoteTokenServices.setClientId(clientId)
		remoteTokenServices.setClientSecret(clientSecret)
		remoteTokenServices.setAccessTokenConverter(jwtTokenEnhancer())  //accessTokenConverter()  jwtTokenEnhancer()

		return remoteTokenServices
	}
	
	@Bean
	protected JwtAccessTokenConverter jwtTokenEnhancer() {
		JwtAccessTokenConverter converter =  new JwtAccessTokenConverter()
		Resource resource = new ClassPathResource("public.cert")
		String publicKey = null
		try {
			publicKey = new String(FileCopyUtils.copyToByteArray(resource.getInputStream()))
		} catch (IOException e) {
			throw new RuntimeException(e)
		}
		converter.setVerifierKey(publicKey)
		return converter
	}
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		// @formatter:off
		resources
				.resourceId(resourceId).tokenServices(remoteTokenServices())
		// @formatter:on
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		// @formatter:off

		http
		.antMatcher("/**")
		.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
		.anyRequest().hasAuthority(defaultRole)
	}
	

}
