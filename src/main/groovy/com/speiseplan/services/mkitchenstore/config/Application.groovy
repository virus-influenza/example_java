package com.speiseplan.services.mkitchenstore.config

import com.speiseplan.services.mkitchenstore.config.amqp.SimpleRabbitMQ
import com.speiseplan.services.mkitchenstore.config.audit.Audit
import com.speiseplan.services.mkitchenstore.config.http.Template
import com.speiseplan.services.mkitchenstore.config.i18n.Internationalization
import com.speiseplan.services.mkitchenstore.config.jdbc.HSQLDataSource
import com.speiseplan.services.mkitchenstore.config.jdbc.MySQLDataSource
import com.speiseplan.services.mkitchenstore.config.pupulate.Populate
import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateCategories
import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateOwners
import com.speiseplan.services.mkitchenstore.config.pupulate.PopulateProd
import com.speiseplan.services.mkitchenstore.config.security.NoSecurityOAuth2ServerConfiguration
import com.speiseplan.services.mkitchenstore.config.security.OAuth2ServerConfiguration
import com.speiseplan.services.mkitchenstore.domain.Category
import com.speiseplan.services.mkitchenstore.processor.FoodPointResourceProcessor
import com.speiseplan.services.mkitchenstore.repository.FoodPointRepository
import com.speiseplan.services.mkitchenstore.util.SimpleCORSFilter
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.orm.jpa.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 *
 * @author virus.influenza
 */
@Configuration
@Import([
        OAuth2ServerConfiguration,
        NoSecurityOAuth2ServerConfiguration,
        HSQLDataSource,
        MySQLDataSource,
        SimpleRabbitMQ,
        Template,
        Internationalization,
        Events,
        Rest,
        Eureka,
        Populate,
        PopulateCategories,
        PopulateOwners,
		PopulateProd,
        Audit,
		Services,
		Web
])
@EnableJpaRepositories(
        basePackageClasses = [FoodPointRepository.class]
)
@EntityScan(
        basePackageClasses = [Category.class]
)
@ComponentScan(basePackageClasses=[
        SimpleCORSFilter,
        FoodPointResourceProcessor,
])
@EnableTransactionManagement
@EnableAutoConfiguration
class Application {
}
