package com.speiseplan.services.mkitchenstore.config

import com.speiseplan.services.mkitchenstore.event.manual.ReindexResourceEventHandler
import com.speiseplan.services.mkitchenstore.service.event.OfferCollectionManualEventTriggerService
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement

@ComponentScan(
        basePackageClasses = [
				OfferCollectionManualEventTriggerService,
                ReindexResourceEventHandler
        ]
)
@EnableTransactionManagement
@Configuration
class Services {
}
