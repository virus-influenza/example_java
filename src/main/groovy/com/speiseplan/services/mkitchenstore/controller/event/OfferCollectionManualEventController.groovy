package com.speiseplan.services.mkitchenstore.controller.event

import com.speiseplan.services.mkitchenstore.model.response.OfferCollectionReindexResponse
import com.speiseplan.services.mkitchenstore.service.event.ManualEventTriggerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
class OfferCollectionManualEventController {

	@Autowired
	ManualEventTriggerService<Long, OfferCollectionReindexResponse> manualEventTriggerService

	@RequestMapping(
			path = '${mkitchenstore.api.resources.incoming.reindex.menu}',
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void reindex(@PathVariable Long id) {
		
		manualEventTriggerService.triggerSingleResourceReindexByPk(id)
	}
}
